/* global module:false */

module.exports = function(grunt) {
  var port = grunt.option('port') || 8000;
  var base = grunt.option('base') || 'slides';

  // Project configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    meta: {
      banner:
        '/*!\n' +
        ' * reveal.js <%= pkg.version %> (<%= grunt.template.today("yyyy-mm-dd, HH:MM") %>)\n' +
        ' * http://lab.hakim.se/reveal-js\n' +
        ' * MIT licensed\n' +
        ' *\n' +
        ' * Copyright (C) 2016 Hakim El Hattab, http://hakim.se\n' +
        ' */'
    },

    uglify: {
      options: {
        banner: '<%= meta.banner %>\n'
      },
      build: {
        src: 'slides/js/reveal.js',
        dest: 'slides/js/reveal.min.js'
      }
    },

    sass: {
      core: {
        files: {
          'slides/css/reveal.css': 'slides/css/reveal.scss'
        }
      },
      themes: {
        files: [
          {
            expand: true,
            cwd: 'slides/css/theme/source',
            src: ['*.scss'],
            dest: 'slides/css/theme',
            ext: '.css'
          }
        ]
      }
    },

    autoprefixer: {
      dist: {
        src: 'slides/css/reveal.css'
      }
    },

    cssmin: {
      compress: {
        files: {
          'slides/css/reveal.min.css': [ 'slides/css/reveal.css' ]
        }
      }
    },

    connect: {
      server: {
        options: {
          port: port,
          base: base,
          livereload: true,
          open: true
        }
      }
    },

    watch: {
      options: {
        livereload: true
      },
      js: {
        files: [ 'Gruntfile.js', 'slides/js/reveal.js' ],
        tasks: 'js'
      },
      theme: {
        files: [
          'slides/css/theme/source/*.scss',
          'slides/css/theme/template/*.scss'
        ],
        tasks: 'css-themes'
      },
      css: {
        files: [ 'slides/css/reveal.scss' ],
        tasks: 'css-core'
      },
      html: {
        files: [ 'slides/*.html' ]
      },
      markdown: {
        files: [ 'slides/*.md' ]
      }
    }

  });

  // Dependencies
  grunt.loadNpmTasks( 'grunt-contrib-qunit' );
  grunt.loadNpmTasks( 'grunt-contrib-jshint' );
  grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
  grunt.loadNpmTasks( 'grunt-contrib-uglify' );
  grunt.loadNpmTasks( 'grunt-contrib-watch' );
  grunt.loadNpmTasks( 'grunt-sass' );
  grunt.loadNpmTasks( 'grunt-contrib-connect' );
  grunt.loadNpmTasks( 'grunt-autoprefixer' );
  grunt.loadNpmTasks( 'grunt-zip' );

  // Default task
  grunt.registerTask( 'default', [ 'css', 'js' ] );

  // JS task
  grunt.registerTask( 'js', [ 'uglify' ] );

  // Theme CSS
  grunt.registerTask( 'css-themes', [ 'sass:themes' ] );

  // Core framework CSS
  grunt.registerTask( 'css-core', [ 'sass:core', 'autoprefixer', 'cssmin' ] );

  // All CSS
  grunt.registerTask( 'css', [ 'sass', 'autoprefixer', 'cssmin' ] );

  // Serve presentation locally
  grunt.registerTask( 'serve', [ 'connect', 'watch' ] );

};
