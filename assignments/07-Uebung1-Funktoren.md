% Funktoren
% Arno Hollosi; Michael Fladischer

Beispiel
--------

Gegeben sei der Datentyp `NamedContainer`, welcher nur einen einzigen Wert `NC` enthält:

~~~~ { .haskell }
data NamedContainer a = NC String a
                        deriving (Show, Eq)
~~~~

D.h. in diesem Datentyp können beliebige andere Datentypen gespeichert werden, zusätzlich wird auch
immer ein String gespeichert. Wir wollen nun die Möglichkeit haben mittels `fmap` den inneren Wert
mit Funktionen zu manipulieren, sodass wir z.B. keine eigene Addition für `NamedContainer Int` schreiben müssen.

~~~~ { .haskell }
-- Lösung
instance Functor NamedContainer where
    -- fmap :: (a -> b) -> NamedContainer a -> NamedContainer b
    fmap f (NC str x) = NC str (f x)

-- Typen
--            f :: a -> b
--          str :: String
--            x :: a
--     NC str x :: NamedContainer a
--        (f x) :: b
-- NC str (f x) :: NamedContainer b
~~~~

Die Funktionssignatur von `fmap` wurde hier für den konkreten Typ zum besseren Verständnis mit angegeben. Die Implementierung der Instanz bricht den zweiten Parameter mithilfe von Pattern-Matching in seine Bestandteile auf: in `NC` (Daten-Konstruktur/Name des Werts), `str` erster Teil (vom Typ `String`) und `x` (vom allgemeinen Typ `a`).

Um den `NamedContainer a` auf den Resultat-Typ `NamedContainer b` zu transformieren, ist es notwendig mithilfe der Funktion `f` **alle** Bestanteile vom Typ `a` in den Typ `b` zu verwandeln. `x` ist der einzige Bestandteil von diesem Typ, deshalb wenden wir `f` auf `x` an: `f x` ist dann vom Typ `b`. Wenn das Ergebnis wieder "verpackt" wird, haben wir den neuen Datentyp `NamedContainer b`.

Beispiel-Aufrufe:

~~~~ { .haskell }
> fmap (+1) (NC "eins" 1)
NC "eins" 2

> fmap length (NC "eins" "zwei")
NC "eins" 4
> :t NC "eins" "zwei"
NamedContainer String
> :t fmap length (NC "eins" "zwei")
NamedContainer Int
~~~~~

Übungen
=======

1. Tupel
--------

Gegeben sei der Datentyp `Tupel`, der zwei Werte mit verschiedenen Datentypen beinhalten kann:

~~~~ { .haskell }
data Tupel c a = T c a
                 deriving (Show, Eq)
~~~~

Schreiben Sie die Funktor-Instanz für den Datentyp (die Signatur ist wieder zur Hilfestellung angegeben):

~~~~ { .haskell }
instance Functor (Tupel c) where
    -- fmap :: (a -> b) -> Tupel c a -> Tupel c b
    fmap ...
~~~~

Tipp: Ein `Tupel`-Wert besteht ähnlich wie ein `NamedContainer`-Wert aus zwei "inneren"  Werten. Dementsprechend sieht auch die Implementierung ähnlich aus.

~~~~ { .haskell }
-- Beispiel-Aufrufe
> fmap sqrt (T "wurzel" 4)
T "wurzel" 2
> fmap length (T False "zwei")
T False 4
~~~~


2. Pair
-------

Gegeben sei der Datentyp `Pair`, der zwei Werte vom gleichen Datentypen enthält. Schreiben Sie für diesen Datentyp eine Funktor-Instanz:

~~~~ { .haskell }
data Pair a = P a a
              deriving (Show, Eq)

instance Functor Pair where
    -- fmap :: (a -> b) -> Pair a -> Pair b
    fmap ...
~~~~

Hinweis: da beide enthaltenen Werte denselben Datentyp `a` haben, kommt `a` in der `data`-Definition links vom `=`-Zeichen nur einmal vor. Achten Sie darauf, dass Sie alle Werte vom Typ `a` in Werte vom Typ `b` umwandeln müssen.

~~~~ { .haskell }
-- Beispiel-Aufrufe:
> fmap sqrt (P 16 9)
P 4 3
> fmap length (P "sieben" "zwei")
P 6 4
~~~~


3. MyList
---------

Gegeben sei der Datentyp `MyList`, der eine Liste nachbildet. Schreiben Sie für diesen Datentyp eine Funktor-Instanz:

~~~~ { .haskell }
data MyList a = Empty | Cons a (MyList a)
                deriving (Show, Eq)

instance Functor MyList where
    -- fmap :: (a -> b) -> MyList a -> MyList b
    fmap ...
~~~~

Hier zeigt sich das erste Mal die Mächtigkeit des Funktor-Konzepts: wir erfinden unsere eigene Datenstruktur und können doch alle bekannten Funktionen darauf anwenden.

Tipp: Unterscheiden Sie mit Pattern-Matching die beiden Fälle und verwenden Sie Rekursion.

Tipp 2: bei der Rekursion haben Sie zwei Bestandteile: einen vom Typ `a` und einen vom Typ `MyList a`. Wenn Sie auf die Signaturen schauen, dann sehen Sie, welche Funktionen geeignet sind, diese Typen in `b` und `MyList b` umzuwandeln.

~~~~ { .haskell }
-- Beispiel-Aufruf:
> fmap (*2) (Cons 1 (Cons 2 (Cons 3 Empty)))
Cons 2 (Cons 4 (Cons 6 Empty))
~~~~

**Bonus-Aufgabe 1**: Schreiben Sie die beiden Funktionen `toList` und `fromList`, die zeigen, dass unser Datentyp äquivalent zur echten Liste ist. Schreiben Sie dann `fmap` nochmal neu, indem Sie `toList` und `fromList` verwenden. (Die so entstehende Funktion ist nicht effizient, soll nur Anwendung verdeutlichen.)

~~~~ { .haskell }
toList :: MyList a -> [a]
fromList :: [a] -> MyList a
~~~~


4. Tree
-------

Gegeben sei der Datentyp `Tree`, der einen einfachen, unsortierten Binär-Baum nachbildet. Schreiben Sie für diesen Datentyp eine Funktor-Instanz:

~~~~ { .haskell }
data Tree a = Leaf | Node (Tree a) a (Tree a)
              deriving (Show, Eq)

instance Functor Tree where
    -- fmap :: (a -> b) -> Tree a -> Tree b
    fmap ...
~~~~

Tipp: die Implementierung ist ähnlich zu der von `MyList`, allerdings gibt es hier zwei Bestandteile, die rekursiv durchgegangen werden müssen. Mit Hilfsvariablen für alle drei Bestanteile sollte es einfach sein, es geht auch, alles in eine Zeile zu schreiben.

~~~~ { .haskell }
-- Beispiel-Aufruf:
> fmap odd (Node (Node Leaf 8 (Node Leaf 7 Leaf)) 3 (Node (Node Leaf 1 Leaf) 2 Leaf))
Node (Node Leaf False (Node Leaf True Leaf)) True (Node (Node Leaf True Leaf) False Leaf)
~~~~
