% Listen
% Arno Hollosi; Michael Fladischer

Listen
-----

Listen sind ein Datentyp, der eine beliebige Anzahl an homogenen Elementen
aufnehmen kann.


Lösungen
========

~~~~  { .haskell }
-- Länge einer Liste von Int berechnen
length' :: [Int] -> Int
length' [] = 0
length' (_:xs) = 1 + length' xs
~~~~


~~~~ { .haskell }
-- Erstes Element einer Liste
head' :: [Char] -> Char
head' [] = error "Empty list"
head' (x:_) = x
~~~~


~~~~ { .haskell }
-- Alle Elemente nach dem ersten Element einer Liste
tail' :: [Int] -> [Int]
tail' [] = error "Empty list"
tail' (_:xs) = xs
~~~~


Übungen
=======


**Übung 5.1** Implemetieren Sie die `init` Funktion, welche alle Elemente bis auf das
letzte einer Liste liefert.

~~~~ { .haskell }
-- Zu verwendende Funktions-Signatur:
init' :: [Int] -> [Int]
~~~~


**Übung 5.2** Implementieren Sie die `last` Funktion, welche das letzte Element einer
Liste zurückliefert.

~~~~ { .haskell }
-- Zu verwendende Funktions-Signatur:
last' :: [Int] -> Int
~~~~


**Übung 5.3** Implementieren Sie die `reverse` Funktion, welche die Reihenfolge der
Elemente in einer Liste umkehrt. Überlegen Sie sich, arum entweder `:` oder `++`
hier nicht anwendbar sind.

~~~~ { .haskell }
-- Zu verwendende Funktions-Signatur:
reverse' :: [Char] -> [Char]
~~~~


**Übung 5.4** Implementieren Sie eine Funktion `range'`, welche zwei `Int`
Parameter engegennimmt. Die Funktion soll eine Liste (*[Int]`) von aufsteigenden
Zahlen liefern, wobei der erste Parameter die Anzahl der Elemente und der zweite
Parameter den Startwert (z.B. 0) angibt.

~~~~ { .haskell }
-- Zu verwendende Funktions-Signatur:
range' :: Int -> Int -> [Int]
~~~~
