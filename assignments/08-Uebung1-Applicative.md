% Applicative
% Arno Hollosi; Michael Fladischer

Beispiel
--------

Gegeben sei der Datentyp `NamedContainer`, welcher nur einen einzigen Wert `NC`
enthält:

~~~~ { .haskell }
data NamedContainer a = NC String a
                        deriving (Show, Eq)
~~~~

D.h. in diesem Datentyp können beliebige andere Datentypen gespeichert werden,
zusätzlich wird auch immer ein String gespeichert. Wir wollen nun die
Möglichkeit haben mittels `<*>` den inneren Wert mit aus Funktionen, welche zwei
oder mehr Parameter erwarten, zu manipulieren. Dafür müssen wir die Typklasse
`Applicative` für den Datentyp implementieren.

**Achtung**: Da `Applicative` die Typklasse `Functor` voraussetzt, nehmen Sie am
besten die Datentypen und Ihre Implementierungen für `Functor` aus Übung 7
(Funktor) als Grundlage. Alle Anmerkungen und Hinweise aus Übung 7 sind auch für
diese Übung vollständig gültig und anwendbar.


~~~~ { .haskell }
-- Lösung:
instance Applicative NamedContainer where
    pure a = NC "" a
    (NC _ f) <*> (NC str x) = NC str (f x)
~~~~


~~~~ { .haskell }
-- Beispiel-Aufrufe:
> pure (+) <*> NC "a" 1 <*> NC "b" 2
NC "b" 3
> pure (++) <*> NC "a" "Hallo" <*> NC "b" "Welt"
NC "b" "HalloWelt"
> fmap (*) (NC "Frage" 21) <*> NC "Antwort" 2
NC "Antwort" 42
~~~~~

Übungen
=======

1. Pair
-------

Gegeben sei der Datentyp `Pair`, der zwei Werte vom gleichen Datentypen enthält. Schreiben Sie für diesen Datentyp eine Applicative-Instanz:

~~~~ { .haskell }
data Pair a = P a a
              deriving (Show, Eq)

instance Applicative Pair where
    pure ... = ...
    ... <*> ... = ...
~~~~


~~~~ { .haskell }
-- Beispiel-Aufrufe:
> pure compare <*> P 1 4 <*> P 0 6
P GT LT
> pure (*) <*> P 1 4 <*> P 0 6
P 0 24
~~~~


2. MyList
---------

Gegeben sei der Datentyp `MyList`, der eine Liste nachbildet. Schreiben Sie für diesen Datentyp eine Applicative-Instanz:

~~~~ { .haskell }
data MyList a = Empty | Cons a (MyList a)
                deriving (Show, Eq)

instance Applicative MyList where
    pure ... = ...
    ... <*> ... = ...
~~~~


~~~~ { .haskell }
-- Beispiel-Aufruf:
> fmap (*) (Cons 1 (Cons 2 (Cons 3 Empty))) <*> Empty
Empty
> fmap (-) (Cons 6 (Cons 4 (Cons 3 Empty))) <*> (Cons 3 (Cons 2 Empty))
Cons 3 (Cons 2 Empty)
~~~~


3. Tree
-------

Gegeben sei der Datentyp `Tree`, der einen einfachen, unsortierten Binär-Baum nachbildet. Schreiben Sie für diesen Datentyp eine Applicative-Instanz:

~~~~ { .haskell }
data Tree a = Leaf | Node (Tree a) a (Tree a)
              deriving (Show, Eq)

instance Applicative Tree where
    pure ... = ...
    ... <*> ... = ...
~~~~


~~~~ { .haskell }
-- Beispiel-Aufruf:
> fmap (*) (Node (Node Leaf 99 Leaf) 2 (Node Leaf 3 Leaf)) <*> (Node Leaf 2 (Node Leaf 4 Leaf))
Node Leaf 4 (Node Leaf 12 Leaf)
~~~~
