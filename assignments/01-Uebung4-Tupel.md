% Tupel & Pattern-Matching
% Arno Hollosi; Michael Fladischer

Tupel
-----

Tupel sind der erste "kombinierte" Datentyp, den wir kennenlernen. Es ist damit möglich, mehrere Werte in einen Datentyp zusammenzufassen.

~~~~ { .haskell }
-- Kombination aus Int und Bool
tupel1 :: (Int, Bool)
tupel1 = (12, True)

-- Kombination aus String, String, Int
tupel2 :: (String, String, Int)
tupel2 = ("Hallo", "Haskell", 42)

-- Verschachtelte Tupel
tupel3 :: ((String, String), String)
tupel3 = (("Homer", "Simpson"), "Springfield")
~~~~

**Achtung** bei der Schreibweise! Ein Funktionsaufruf wird ohne Klammern gemacht, z.B. "`sumSquare 3 4`", Tupel werden in runde Klammern eingeschlossen und haben einen Beistrich, z.B. "`sumSquare2 (3,4)`". Die Datentypen dieser beiden Funktionen sehen daher wie folgt aus:

~~~~ { .haskell }
-- 2 Parameter, jeweils Int
sumSquare :: Int -> Int -> Int
--         erster  zweiter

-- nur 1 Parameter vom Typ Tupel
sumSquare2 :: (Int, Int) -> Int
--            \ erster /
--         und einziger Parameter
~~~~

Übungen
=======

**Übung 4.1** Schreiben Sie die Funktion `nachname` (inkl. Signatur), welche den Nachnamen aus folgenden Tupeln zurückliefert und dabei Pattern-Matching verwendet:

~~~~  { .haskell }
nachname (("Homer", "Simpson"), "Springfield")
> "Simpson"

nachname (("Montgomery", "Burns"), "Springfield")
> "Burns"
~~~~

**Übung 4.2** Schreiben Sie die Funktion `nachname2` (inkl. Signatur), welche identisch ist mit der Funktion `nachname`, verwenden Sie aber *kein* Pattern-Matching, sondern die Funktionen `fst` und `snd` (beide Funktionen müsssen 1x vorkommen).

**Übung 4.3** Schreiben Sie zu den folgenden beiden Funktionen jeweils eine passende Signatur (alle Werte sind vom Typ `String`, mit Ausnahme von `plz` welcher vom Typ `Int` ist):

~~~~  { .haskell }
mkPerson name nachName ort = ((name, nachName), ort)
mkPerson2 (name,plz) nachName = ((name, nachName), plz)

-- Beispielaufrufe
mkPerson "Isaac" "Newton" "Kensington"
mkPerson2 ("Werner", 80331) "Heisenberg"
~~~~

**Übung 4.4** Im folgenden Beispiel haben sich Fehler eingeschlichen. Beheben Sie die Fehler.

~~~~  { .haskell }
flaecheQuadrat :: Int -> Int -> Int
flaecheQuadrat a b = a * b

volumenWürfel :: (Int, Int) -> Int -> Int
volumenWürfel (a,b) c = flaecheQuadrat(a, b) * c

volumenPyramide :: (Int, Int, Int) -> Int
volumenPyramide a b c = volumenWürfel(a,b,c) `div` 3

volumenHaus :: (Int, Int) -> (Int, Int) -> Int
volumenHaus ((a,b), (c,d)) = (volumenWürfel a b c) + (volumenPyramide a b d)
~~~~
