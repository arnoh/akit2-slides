% Funktionen höherer Ordnung
% Arno Hollosi; Michael Fladischer

Übung 1
-------

Vereinfachen Sie folgende Funktionen mit `map`:

~~~~  { .haskell }
data Person  = Pers { name  :: String
                    , alter :: Int
                    , plz   :: Int
                    }
                    deriving (Show)

names :: [Person] -> [String]
names [] = []
names (p:ps) = name p : names ps

steiermark :: [Person] -> [Bool]
steiermark [] = []
steiermark (p:ps) = stmk p : steiermark ps
    where
        stmk person = (plz person >= 8000) && (plz person < 9000)

übersiedeln :: Int -> Int -> [Person] -> [Person]
übersiedeln vonPLZ nachPLZ [] = []
übersiedeln vonPLZ nachPLZ (p:ps) = siedeln p : (übersiedeln vonPLZ nachPLZ ps)
    where
        siedeln person =
            if plz person == vonPLZ
                then person { plz = nachPLZ }
                else person

volljährig :: [Person] -> [Bool]
volljährig ps =
    if null ps
        then []
        else
            let
                person = head ps   -- erstes Element
                psNeu  = tail ps   -- restliche Personen
            in
                (alter person >= 18) : volljährig psNeu
~~~~

Folgende Beispieldaten können Sie zum Testen verwenden:

~~~~ { .haskell }
personen :: [Person]
personen = [ Pers "Franz H." 17 8123
           , Pers "Rosi S."  33 1016
           , Pers "Hans K."  44 8531
           , Pers "Susi M."  12 6322
           , Pers "Doris D." 68 3104
           , Pers "Fredl B."  9 6322
           ]
~~~~


Übung 2
-------

Schreiben Sie folgende Funktionen mithilfe von `map` (d.h. ohne explizite Rekursion):

~~~~ { .haskell }
data Class = First | Business | Economy | Standing
             deriving (Show, Eq)

data Meal = Omnivore | Vegetarian | Vegan | None
            deriving (Show, Eq)

data PassengerT = Passenger { fullname :: String
                            , seat     :: Class
                            , meal     :: Meal
                            , luggage  :: Int
                            }
                            deriving (Show)

-- Passagiere, die First-Class fliegen
firstClass :: [PassengerT] -> [Bool]

-- Passagiere, welche eine bestimmte Mahlzeit einnehmen
eating :: Meal -> [PassengerT] -> [Bool]

-- Wie viel kg Gepäck, die Passagiere zuviel mithaben, d.h. über 20kg
-- Bsp.: 15kg Gepäck -> 0, 20kg Gepäck -> 0, 26kg Gepäck -> 6kg
overweight :: [PassengerT] -> [Int]

-- Umbuchen auf andere Klasse
--          von  ->  nach
upgrade :: Class -> Class -> [PassengerT] -> [PassengerT]

-- bekommt zwei Funktionen übergeben:
--   1) bildet PassengerT auf einen Zwischen-Typ "a" ab, z.B. Meal
--   2) Test, ob Zwischen-Typ eine bestimmte Bedingung erfüllt
checkPassengers :: (PassengerT -> a) -> (a -> Bool) -> [PassengerT] -> [Bool]

-- Schreiben Sie die Funktionen firstClass & eating neu,
-- diesmal mit Verwendung von checkPassengers, d.h. ohne map
firstClassCP :: [PassengerT] -> [Bool]
eatingCP :: Meal -> [PassengerT] -> [Bool]
~~~~

Folgende Testdaten können Sie verwenden:

~~~~ { .haskell }
passengers :: [PassengerT]
passengers = [ Passenger "Franz H." First    Omnivore   17
             , Passenger "Rosi S."  Standing None       20
             , Passenger "Hans K."  Economy  Vegan       5
             , Passenger "Susi M."  Business Vegan      22
             , Passenger "Doris D." First    Omnivore   27
             , Passenger "Fredl B." Standing Vegetarian 13
             ]
~~~~
