% ADT & Pattern-Matching
% Arno Hollosi; Michael Fladischer

Übungen
=======

Übung 6.1
---------

Schreiben Sie für Datentyp `Person`, die angeführten Funktionen:

~~~~  { .haskell }
data Name    = Nm String
data Alter   = Jahre Int
data Adresse = Adr String
data Person  = Pers Name Alter Adresse

name  :: Name -> String
alter :: Alter -> Int

pName   :: Person -> String
pAlter  :: Person -> Int
adresse :: Person -> String

mkPerson  :: String -> Int -> String -> Person -- warum ist diese Funktion schlecht?
mkPerson2 :: Name -> Alter -> Adresse -> Person -- warum ist diese Funktion besser?
~~~~


Übung 6.2
---------

Bauen Sie Ihren eigenen Boolschen-Datentyp: `data Boolean = Wahr | Falsch` und schreiben Sie die folgenden Funktionen:

~~~~ { .haskell }
und   :: Boolean -> Boolean -> Boolean
oder  :: Boolean -> Boolean -> Boolean
nicht :: Boolean -> Boolean
xor   :: Boolean -> Boolean -> Boolean
~~~~

Hinweis:

* Erzeugen Sie `xor` mithilfe der drei vorhergehenden Funktionen, nach folgender Formel: 
  $p \oplus q = (p \lor q) \land  \lnot (p \land q)$
* Sie können auch eine zweite Variante schreiben, Formel:
  $p \oplus q = (p \land \lnot q) \lor (\lnot p \land q)$


Übung 6.3
---------

Schreiben Sie einen Datentyp `Form`. Er soll drei mögliche Werte enthalten:

* `Dreieck`, welches die Seitenlänge von drei Seiten enthält
* `Quadrat`, welches eine Angabe zur Seitenlänge hat
* `Rechteck`, welches zwei Seitenlängen enthält

Schreiben Sie dann folgende zwei Funktionen inkl. Signaturen:

* `umfang`, welche den Umfang einer Form berechnet
* `flaeche`, welche die Fläche einer Form berechnet

Hinweise:

* `Form` ist ein Summen-Typ, d.h. es ist nur eine einzige `data`-Definition notwendig)
* [Formel](https://de.wikipedia.org/wiki/Dreiecksfl%C3%A4che) für die Flächenberechnung des Dreiecks; da Sie hier die Wurzelfunktion `sqrt` brauchen, sollten die Zahlen vom Typ `Float` sein.)
* Verwenden Sie beim Dreieck die Funktion `umfang` innherhalb der `flaeche` wieder (Sie benötigen den Umfang zur Berechnung).
