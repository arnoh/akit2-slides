% Rekursion & Pattern-Matching
% Arno Hollosi; Michael Fladischer

Rekursion
---------

In Haskell gibt es wegen der Purität keine Schleifen. Diese werden stattdessen mit Rekursion umgesetzt. Eine Rekursion hat immer (min.) einen Berechnungsschritt und (min.) eine Abbruchbedingung.

Es gibt drei Rekursionsmuster, die häufig anzutreffen sind:

* **Tail-Recursion**: der letzte (möglicherweise einzige) Schritt bei einer Berechnung ist der Aufruf der Funktion mit sich selbst - **ohne** dass der Aufruf mit anderen Operationen verknüpft ist.
* Rekursion mit **Akkumulator**: das Ergebnis wird in einer Hilfsvariable gesammelt. Um diese in der Rekursion benützen zu können, wird eine Hilfsfunktion definiert, welche dieselben Parameter + die Hilfsvariable als Parameter hat.
* **Nibble-Recursion**: "knabbert" (to nibble) den Inputwert stückweise an. Das Ergebnis wird schrittweise aus aktuellem Wert + Ergebnis (Rekursion) des "Restwerts" berechnet.


Beispiele
---------

**Tail-Recursion**

~~~~ { .haskell }
ggT :: Int -> Int -> Int
ggT x 0 = x                  -- Abbruchbedingung
ggT x y = ggT y (x `mod` y)  -- rekursiver Schritt

-- mit Hilfsvariablen
ggT2 :: Int -> Int -> Int
ggT2 x 0 = x                 -- Abbruchbedingung
ggT2 x y =
    let
        xNeu = y
        yNeu = x `mod` y
    in
        ggT2 xNeu yNeu       -- hier gut zu sehen: nur Funktion selbst wird aufgerufen
~~~~

**Nibble-Recursion**

~~~~ { .haskell }
pot :: Float -> Int -> Float
pot n 0 = 1
pot n e = n * pot n (e - 1)
--        \_/ \___________/
-- aktueller & restlicher Wert (e ist um 1 kleiner)
~~~~

**Rekursion mit Akkumulator**

~~~~ { .haskell }
-- zuerst Nibble-Rekursion ohne Akkumulator
factNib :: Integer -> Integer
factNib 0 = 1
factNib n = n * factNib (n-1)

-- Hilfsfunktion mit einem Parameter (Akkumulator) mehr
helper :: Integer -> Integer -> Integer
helper 0 acc = acc -- bei Abbruchbedingung wird Akkumulator retourniert
helper n acc =
    let
        nNeu = n - 1        -- anknabbern des restlichen Inputs
        accNeu = n * acc    -- Ergebnis wird in Akkumulator berechnet
    in
        helper nNeu accNeu  -- Tail-Rekursion dank Akkumulator

-- nun Tail-Rekursion, dank helper-Funktion
factAcc :: Integer -> Integer
factAcc n = helper n 1      -- 1 ist "Startwert" des Akkumulators
                            -- (Multiplikation mit 1 aendert Ergebnis nicht)

-- alles in einer Funktion
fact :: Integer -> Integer
fact 0 = 1
fact n = accFact n 1
    where
        accFact 0 acc = acc
        accFact n acc = accFact (n-1) (n*acc)
~~~~


Übungen
=======

**Übung 3.1** Wandeln Sie die Nibble-Rekursion der oben angeführten Potenz-Funktion `pot` in eine Akkumulator-Rekursion um.

**Übung 3.2** Schreiben Sie die Funktion `crossSum`, welche die Quersumme einer Integer-Zahl berechnet. Benützen Sie `div` und `mod` um an die einzelnen Ziffern der Zahl zu gelangen. Denken Sie über Abbruchbedingung und Rekursionsschritt nach.

~~~~  { .haskell }
crossSum 14
> 5

crossSum 753925243763839873611001
> 103
~~~~

**Übung 3.3** Schreiben Sie eine Funktion `reverseInt`, welche eine Integer-Zahl umdreht. Sie benötigen wieder `div` und `mod`. Der Rekursionsschritt enthält auch eine Multiplikation. Tipp: am einfachsten ist die Funktion mit einer Akkumulator-Rekursion umzusetzen.

~~~~  { .haskell }
reverseInt 324623649123012933
> 339210321946326423
~~~~

**Übung 3.4** Berechnen Sie $\pi$ mithilfe der Eulerschen Reihe:
$\frac{1}{1^2} + \frac{1}{3^2} + \frac{1}{5^2} + \frac{1}{7^2} + ... = \frac{\pi^2}{8}$
Die Funktion soll als Parameter die gewünschte Anzahl an Iterationen erhalten. Tipp: am einfachsten ist die Funktion mit einer Akkumulator-Rekursion umzusetzen. Sie können sie auch alternativ/zusätzlich mithilfe einer Nibble-Rekursion umsetzen. (Falls notwendig: Sie können mit `fromIntegral` eine `Int`-Zahl in eine `Float`-Zahl umrechnen.)

~~~~ { .haskell }
calcPiEuler 1
> 2.828427

calcPiEuler 2
> 2.981424

calcPiEuler 100
> 3.138408

calcPiEuler 500000
> 3.1414597
~~~~


**Übung 3.5** Berechnen Sie $\pi$ mithilfe der [Leibniz-Reihe](https://de.wikipedia.org/wiki/Leibniz-Reihe "Wikipedia: Leibniz-Reihe"):
$1 - \frac{1}{3} + \frac{1}{5} - \frac{1}{7} + \frac{1}{9} - ... = \frac{\pi}{4}$
Die Funktion soll als Parameter die gewünschte Anzahl an Iterationen erhalten. Tipp: die Funktion ist recht ähnlich zur Eulerschen Reihe, die Hilfsfunktion benötigt aber einen Parameter mehr.

~~~~ { .haskell }
calcPiLeibnitz 1
> 2.6666665

calcPiLeibnitz 2
> 3.4666665

calcPiLeibnitz 100
> 3.151493

calcPiLeibnitz 500000
> 3.141598
~~~~

