% foldl/foldr
% Arno Hollosi; Michael Fladischer

Übung 1
-------

Vervollständigen Sie den Funktionsbody zur Signatur. Die Funktion soll aus einer Liste von `Real a` den größten Wert ermitteln. Verwenden Sie dazu `foldl`/`foldr` sowie entweder `max` oder eine anonyme Funktion.

~~~~ { .haskell }
findeMaximum :: (Real a) => [a] -> a
~~~~

Verallgemeinern Sie die Funktion (+ Signatur) so, dass sie für alle Datentypen mit Reihenfolge funktioniert.

Was muss an der Funktion verändert werden, damit Sie das Minimum ermittelt?

Übung 2
-------

Implementieren Sie eine Funktion, die ermittelt, wie oft ein bestimmtes `Char` in einem `String` vorkommt.
Verwenden Sie dazu `foldl` oder `foldr`.

~~~~ { .haskell }
zähleChar :: Char -> String -> Int
~~~~

Wie muss die Funktion (oder Signatur) verändert werden, damit beliebige Elemente in einer Liste ebensolcher Elemente gezählt werden können?

(Hinweis: `String` ist ein Synonym für `[Char]`)