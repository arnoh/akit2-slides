% Monaden
% Arno Hollosi; Michael Fladischer

Beispiele
---------


Gegeben seien folgende "sichere" Rechenfunktionen:

~~~~ { .haskell }
safeSqrt :: Float -> Maybe Float
safeSqrt x = if x >= 0
               then Just (sqrt x)
               else Nothing

safeLog :: Float -> Maybe Float
safeLog x = if x > 0
              then Just (log x)
              else Nothing

safeDiv :: Float -> Float -> Maybe Float
safeDiv x 0 = Nothing
safeDiv x y = Just (x / y)
~~~~

Gesucht ist die Funktion $f(x) = \frac{x}{\sqrt \log x}$.
Eine Verkettung der einzelnen Funktionen ist mühsam, weil man das Ergebnis immer auf `Nothing` prüfen muss:

~~~~ { .haskell }
safeCalc :: Float -> Maybe Float
safeCalc x =
    case safeLog x of
        Nothing   -> Nothing
        (Just x2) ->
            case safeSqrt x2 of
                Nothing   -> Nothing
                (Just x3) -> safeDiv x x3
~~~~

Wesentlich hübscher wäre es, wenn wir eine Bind-Funktion hätten, mit der Signatur `m a -> (a -> m b) -> m b`. "`m a`" steht dabei für eine "umgebende bzw. einbettende" Struktur `m` und einen "innenliegenden bzw. eingebetteten" Datentyp `a`. Beispiele:

* `Maybe Int           -- m = Maybe,         a = Int`
* `[Float]             -- m = [],            a = Float`
* `Either String Date  -- m = Either String, a = Date`

In unserem Fall suchen wir also eine Bind-Funktion mit der Signatur `Maybe Float -> (Float -> Maybe Float) -> Maybe Float`, d.h. in unserem Fall is Datentyp `a` gleich Datentyp `b`. Eine Bind-Funktion für `Maybe` würde so aussehen:

~~~~ { .haskell }
bindMaybe :: Maybe a -> (a -> Maybe b) -> Maybe b
bindMaybe ma f =
    case ma of
       Nothing  -> Nothing
        (Just x) -> f x
~~~~

Diese Funktion erlaubt es uns, Aufrufe zu verketten. `safeCalc` würde dann so aussehen:

~~~~ { .haskell }
safeCalc2 :: Float -> Maybe Float
safeCalc2 x = safeLog x `bindMaybe` safeSqrt `bindMaybe` safeDiv x
~~~~

Haskell definiert eine eigene Typklasse für Datenstrukturen, die eine solche Verkettung erlauben: Monaden. Dort heißt der Bind-Operator `(>>=)`, d.h. `safeCalc` würde man so schreiben:

~~~~ { .haskell }
safeCalc3 :: Float -> Maybe Float
safeCalc3 x = safeLog x >>= safeSqrt >>= safeDiv x
~~~~

Der Operator gibt die "Richtung" des Datenflusses an. Was auffäult: wir müssen uns nicht mehr um die Fehlerfälle `Nothing` kümmern, weil das im Operator implizit enthalten ist (siehe `bindMaybe` oben). Es gibt eine weitere Notation für Monaden, die recht praktisch ist, vor allem, wenn die Berechnungen komplizierter werden. Die `do`-Notation, eingeleitet mit gleichnamigen Schlüsselwort:

~~~~ { .haskell }
safeCalc4 :: Float -> Maybe Float
safeCalc4 x = do
    y <- safeLog x
    z <- safeSqrt y
    safeDiv x z
~~~~

Der Operator `<-` kann gelesen werden als "holt Wert aus Struktur heraus". Bei uns also "holt `Float`-Wert aus `Maybe`-Struktur heraus".

Übungen
=======

1. Weitere Berechnung
---------------------

Gesucht ist die Funktion $f(x) = \frac{\frac{x}{\sqrt \log x}}{abs(1 - x)}$.
Berechnen Sie die Funktion mithilfe von `safeLog`, `safeDiv`, und `safeSqrt`.

Schreiben Sie die Funktion auf drei Arten:

* "zu Fuss"
* mit `(>>=)`-Operator
* mit do-Notation


2. Protokollierte Berechnung
----------------------------

Gegeben seien folgende Typen und Funktionen:

~~~~ { .haskell }
data Logged a = Log [String] a

loggedAdd :: (Num a, Show a) => a -> a -> Logged a
loggedAdd x y = Log ["Addition von "++show x++" und "++show y] (x + y)

loggedMul :: (Num a, Show a) => a -> a -> Logged a
loggedMul x y = Log ["Multiplikation von "++show x++" und "++show y] (x * y)

loggedNeg :: (Num a, Show a) => a -> Logged a
loggedNeg x = Log ["Negieren von "++show x] (negate x)
~~~~

Schreiben Sie die **Funktor-Instanz** für den Datentyp `Logged` (die Signatur ist wieder zur Hilfestellung angegeben):

~~~~ { .haskell }
instance Functor Logged where
    -- fmap :: (a -> b) -> Logged a -> Logged b
    fmap ...
~~~~

Schreiben Sie die **Applicative-Instanz** für den Datentyp `Logged`. Hinweise:

* bei `pure` verwenden Sie die minimal mögliche Variante, den neuen Typ zu erzeugen
* bei `(<*>)` haben Sie zwei String-Listen (eine von jedem Parameter). Die beiden sollten miteinander verknüpft werden.
* Überprüfen Sie, ob folgende Aufrufe bei Ihnen `True` liefern:
    * `(pure id <*> Log ["Start"] 1) == Log ["Start"] 1`
    * `(pure (.) <*> Log ["Inc"] (+1) <*> Log ["Mul"] (*2) <*> Log ["Vier"] 4) == (Log ["Inc"] (+1) <*> (Log ["Mul"] (*2)`

~~~~ { .haskell }
instance Applicative Logged where
    -- pure :: a -> Logged a
    pure ...
    -- (<*>) :: Logged (a -> b) -> Logged a -> Logged b
    <*> ...
~~~~


Schreiben Sie die **Monaden-Instanz** für den Datentyp `Logged`. Hinweise:

* `return` hat dieselbe Signatur (und dieselben Eigenschaften) wie `pure`.
* `(>>=)` auch hier haben Sie zwei String-Listen, die Sie miteinander verknüpfen sollten.
* Überprüfen Sie, ob folgende Aufrufe bei Ihnen `True` liefern:
    * `(return 7 >>= loggedAdd 3)  ==  loggedAdd 3 7`
    * `(loggedAdd 4 5 >>= return)  ==  loggedAdd 4 5`
    * `((loggedNeg 4 >>= loggedMul 3) >>= loggedAdd 12)  ==  (loggedNeg 4 >>= (\x -> loggedMul 3 x >>= loggedAdd 12))`

~~~~ { .haskell }
instance Monad Logged where
    -- return :: a -> Logged a
    return ...
    -- (>>=) :: Logged a -> (a -> Logged b) -> Logged b
    >>= ...
~~~~

**a)** Schreiben Sie eine Funktion $f(x) = (x*3) + 4$. Einmal "zu Fuss", einmal mit `(>>=)` und einmal mit do-Notation. 

**b)** Schreiben Sie eine Funktion $f(x,y) = (x*3) - y$. Einmal "zu Fuss", einmal mit `(>>=)` (Tipp: anonyme Funktionen sind hilfreich) und einmal mit do-Notation.
