-- Funktionen aus Übung 1
square :: Int -> Int
square x = x * x

addTwo :: Int -> Int -> Int
addTwo x y = x + y



-- Funktion, die Zahlen quadriert, dann addiert
-- Verwenden Sie addTwo, square und let:
-- weisen Sie das Ergebnis von square Hilfsvariablen zu
letSumSquare :: Int -> Int -> Int
letSumSquare x y =
    let                -- "let" leitet die Definitionen ein
        x2 = square x
        y2 = square y
    in                 -- im "in" können dann diese Definitionen verwendet werden
        addTwo x2 y2

-- Funktion, die Zahlen quadriert, dann addiert
-- Verwenden Sie addTwo, square und where:
-- weisen Sie das Ergebnis von square Hilfsvariablen zu
whereSumSquare :: Int -> Int -> Int
whereSumSquare x y = addTwo x2 y2
    where
        x2 = square x
        y2 = square y

-- Satz von Pythagoras prüfen (a² + b² = c²)
-- Verwenden Sie sumSquare und square
pythagorasLet :: Int -> Int -> Int -> Bool
pythagorasLet a b c =
    let
        ab2 = letSumSquare a b
        c2  = square c
    in
        ab2 == c2


-- Satz von Pythagoras prüfen (a² + b² = c²)
-- Verwenden Sie sumSquare und square
pythagorasWhere :: Int -> Int -> Int -> Bool
pythagorasWhere a b c = ab2 == c2
    where
        ab2 = letSumSquare a b
        c2  = square c

-- Berechnet Fläche eines Dreiecks, wenn Länge der drei Seiten gegeben ist
fläche3 :: Float -> Float -> Float -> Float
fläche3 a b c =
    let s = (a + b + c) / 2
        f2 = s * (s - a) * (s - b) * (s - c)
    in sqrt f2
