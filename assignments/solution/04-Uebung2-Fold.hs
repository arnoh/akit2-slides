
-- Übung 1
-- Die Funktion soll aus einer Liste von Zahlen den größten Wert ermitteln.
-- Verwenden Sie dazu foldl/foldr sowie entweder max oder eine anonyme Funktion.

-- einfache Variante, funktioniert aber nicht mit negativen Zahlen (liefert 0).
findeMaximum :: (Real a) => [a] -> a
findeMaximum xs = foldr max 0 xs

-- Verallgemeinern Sie die Funktion (+ Signatur) so, dass sie für alle Datentypen mit Ord-Instanz funktioniert.
-- funktioniert auch für negative Zahlen // könnte auch mit foldl geschrieben werden
-- wir nehmen gleich das erste Element der Liste für den max-Vergleich
-- bei einer leeren Liste müssen wir dann aber einen Fehler werfen
findeMaximum2 :: (Ord a) => [a] -> a
findeMaximum2 []     = error "leere Liste!"
findeMaximum2 (x:xs) = foldr max x xs

-- Hinweis: weil diese Variante recht häufig gebraucht wird, gibt es eigene Hilfsfunktionen,
-- welche das erste Element als Startwert des Folds nehmen: foldl1, foldr1
findeMaximum3 :: (Ord a) => [a] -> a
findeMaximum3 xs = foldr1 max xs


-- Beispiel
data Day = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday
           deriving (Show, Eq, Ord)
-- das deriving Ord erzeugt uns automatisch eine Ord-Instanz für unseren Datentyp.
-- Die Werte sind einfach von links nach rechts aufsteigend, d.h.
-- Monday < Tuesday, Saturday > Friday, etc.
--
-- Beispiel-Aufruf:
-- findeMaximum2 [Wednesday, Friday, Thursday] == Friday


-- Was muss an der Funktion verändert werden, damit Sie das Minimum ermittelt?
findeMinimum :: (Ord a) => [a] -> a
findeMinimum []     = error "leere Liste!"
findeMinimum (x:xs) = foldr min x xs



-- Übung 2

-- Implementieren Sie eine Funktion, die ermittelt, wie oft ein bestimmtes Char in einem String vorkommt.
-- Verwenden Sie dazu foldl oder foldr.

zähleChar :: Char -> String -> Int
zähleChar ch str = foldr cnt 0 str
    where
        cnt x acc = if x == ch
                        then acc + 1
                        else acc

-- Wie muss die Funktion (oder Signatur) verändert werden, damit beliebige Elemente in
-- einer Liste ebensolcher Elemente gezählt werden können?
--
-- String ist Synonym für [Char], d.h. wir hatten eigentlich folgende Signatur: Char -> [Char] -> Int
-- Für beliebige Elemente: wir machen aus "Char" die Typvariable a.
-- Da wir das "==" benötigen, schränken wir "a" auf die Typklasse "Eq" ein.
zähleChar2 :: (Eq a) => a -> [a] -> Int
zähleChar2 x xs = foldr cnt 0 xs
    where
        cnt y acc = if y == x
                        then acc + 1
                        else acc
