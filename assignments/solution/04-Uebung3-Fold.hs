myFilter :: (a -> Bool) -> [a] -> [a]
myFilter f []     = []
myFilter f (x:xs) = if f x
                      then x : myFilter f xs -- x ist dabei
                      else     myFilter f xs -- x ist nicht dabei

myFilter2 :: (a -> Bool) -> [a] -> [a]
myFilter2 f xs = foldr test [] xs
    where
        test x acc = if f x
                       then x : acc -- x ist dabei
                       else     acc -- x ist nicht dabei

-- Übung 1

-- mit Fold
delete :: (Eq a) => a -> [a] -> [a]
delete y xs = foldr remove [] xs
    where
        remove x acc = if x == y
                         then acc
                         else x : acc

-- mit Filter
deleteF :: (Eq a) => a -> [a] -> [a]
deleteF y xs = filter (\x -> x /= y) xs

-- rekursiv
deleteR :: (Eq a) => a -> [a] -> [a]
deleteR y [] = []
deleteR y (x:xs) = if x == y
                     then     deleteR y xs
                     else x : deleteR y xs

-- Übung 2

data Obst = Apfel | Birne | Orange | Banane
            deriving (Show, Eq)

-- eine Kiste enthält eine bestimme Anzahl eines Obstes
data Kiste = KisteW { obst  :: Obst
                    , stück :: Int
                    }
                    deriving (Show)

-- mit Fold und Hilfsfunktion
summeObst :: Obst -> [Kiste] -> Int
summeObst sorte kisten = foldl zählen 0 kisten
    where
        zählen summe kiste =
            if obst kiste == sorte
              then summe + stück kiste
              else summe

-- mit map, filter, und sum
summeObst2 :: Obst -> [Kiste] -> Int
summeObst2 sorte kisten =
    let
        passendeKisten = filter (\k -> obst k == sorte) kisten
        stückListe = map stück passendeKisten
    in
        sum stückListe

-- oder in einer Zeile mit Komposition
summeObst3 :: Obst -> [Kiste] -> Int
summeObst3 sorte kisten = sum . map stück . filter (\k -> obst k == sorte) $ kisten



-- Übung 3

data MyList a = Empty | Cons a (MyList a)
                deriving (Show, Eq)

-- die Foldable-Instanz für MyList sieht der Listen-Instanz sehr ähnlich
instance Foldable MyList where
    -- foldr :: (a -> b -> b) -> b -> MyList a -> b
    foldr f z Empty       = z
    foldr f z (Cons x xs) = f x (foldr f z xs)

    -- Implementierung mit Monid/foldMap (foldr kann dann weggelassen werden)
    -- foldMap :: (Monoid m) => (a -> m) -> MyList a -> m
    foldMap f Empty       = mempty
    foldMap f (Cons x xs) = mappend (f x) (foldMap f xs)


-- Übung 4

data Tree a = Leaf | Node (Tree a) a (Tree a)
              deriving (Show, Eq)

instance Foldable Tree where
    -- foldr :: (a -> b -> b) -> b -> Tree a -> b
    foldr f acc Leaf         = acc
    foldr f acc (Node l v r) =
        let -- der Akkumulator bzw. Nullwert wird stückweise durchgeschleift
            acc2 = foldr f acc l
            acc3 = f v acc2
        in
            foldr f acc3 r

    -- Implementierung mit Monid/foldMap (foldr kann dann weggelassen werden)
    -- foldMap :: (Monoid m) => (a -> m) -> Tree a -> m
    foldMap f Leaf         = mempty
    foldMap f (Node l v r) = (foldMap f l) `mappend` (f v) `mappend` (foldMap f r)
