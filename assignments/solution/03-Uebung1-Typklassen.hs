-- Datentyp der zwei Nummern enhält: Vorwahl und Anschluss
data Nummer = Nr (Int, Int)

instance Show Nummer where
    show (Nr (vorwahl, nr)) = "+43/"++(show vorwahl)++"/"++(show nr)
    -- wir verwenden "show vorwahl", um die Int-Zahl in einen String zu verwandel

-- Datentyp für den Typ der Organisation
data Typ = Firma | Person | Service | Geheim

instance Show Typ where
    -- mithilfe von Pattern-Matching weisen wir den einzelnen Werten einen passenden String zu
    show Firma = "Firma"
    show Person = "Person"
    show Service = "Service"
    show Geheim = "Geheim"

-- Eintrag in einem Telefonbuch oder einer Liste
data Eintrag = Etrg Typ String Nummer

instance Show Eintrag where
    show (Etrg typ name nummer) = name++" "++(show nummer)++" ("++(show typ)++")"

-- Beispiel:
-- > Etrg Person "Alice" (Nr (555, 5678))
-- Alice +43/555/5678 (Person)
-- > show (Etrg Geheim "Bob" (Nr (123, 4567)))
-- "Bob +43/123/4567 (Geheim)"
