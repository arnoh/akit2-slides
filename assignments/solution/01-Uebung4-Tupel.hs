-- Übung 4.1
nachname :: ((String, String), String) -> String
nachname ((vn, nn), ort) = vn
-- Struktur der Parameter spiegelt sich exakt in Struktur der Signatur wieder

-- Übung 4.2
nachname2 :: ((String, String), String) -> String
nachname2 person = snd (fst person)
-- Zuerst wird "fst person" berechnet, das Ergebnis davon wird an "snd" übergeben
{- Deutlicher mit Hilfsvariablen

nachname2 person =
    let
        vollerName = fst person
    in
        snd vollerName
-}

-- Übung 4.3
mkPerson :: String -> String -> String -> ((String, String), String)
mkPerson name nachName ort = ((name, nachName), ort)

mkPerson2 :: (String, Int) -> String -> ((String, String), Int)
mkPerson2 (name,plz) nachName = ((name, nachName), plz)

-- Übung 4.4
-- eine mögliche Auflösung (Funktionssignaturen nicht verändert)
flaecheQuadrat :: Int -> Int -> Int
flaecheQuadrat a b = a * b

volumenWürfel :: (Int, Int) -> Int -> Int
volumenWürfel (a, b) c = (flaecheQuadrat a b) * c

volumenPyramide :: (Int, Int, Int) -> Int
volumenPyramide (a, b, c) = (volumenWürfel (a,b) c) `div` 3

volumenHaus :: (Int, Int) -> (Int, Int) -> Int
volumenHaus (a,b) (c,d) = (volumenWürfel (a,b) c) + (volumenPyramide (a,b,d))

{- Eine alternative Auflösung (Signaturen teilweise verändert)

flaecheQuadrat :: (Int, Int) -> Int
flaecheQuadrat (a, b) = a * b

volumenWürfel :: (Int, Int, Int) -> Int
volumenWürfel (a,b,c) = flaecheQuadrat (a, b) * c

volumenPyramide :: (Int, Int, Int) -> Int
volumenPyramide (a,b,c) = volumenWürfel (a,b,c) `div` 3

volumenHaus :: (Int, Int) -> (Int, Int) -> Int
volumenHaus (a,b) (c,d) = volumenWürfel (a,b,c) + volumenPyramide (a,b,d)
-}