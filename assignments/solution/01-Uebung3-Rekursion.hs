ggT :: Int -> Int -> Int
ggT x 0 = x                  -- Abbruchbedingung
ggT x y = ggT y (x `mod` y)  -- rekursiver Schritt
{-
Wichtig beim Pattern-Matching:
Haskell arbeitet die Patterns von oben nach unten ab, das erste zutreffende wird genommen.

Die Funktion
> ggT x y = ggT y (x `mod` y)  -- rekursiver Schritt
> ggT x 0 = x                  -- Abbruchbedingung
würde nie fertig werden (Endlos-Schleife), weil das erste Pattern immer zutrifft
-}


-- mit Hilfsvariablen
ggT2 :: Int -> Int -> Int
ggT2 x 0 = x                 -- Abbruchbedingung
ggT2 x y =
    let
        xNeu = y
        yNeu = x `mod` y
    in
        ggT2 xNeu yNeu       -- hier gut zu sehen: nur Funktion selbst wird aufgerufen


pot :: Float -> Int -> Float
pot n 0 = 1
pot n e = n * pot n (e - 1)
--        \_/ \___________/
-- aktueller & restlicher Wert (e ist um 1 kleiner)
-- typische Nibble-Rekursion: wir haben von der Zahl 1 weggeknabbert
-- frei nach dem Motto: "wenn ich nur aufhören könnte..."
--                      (können wir, wegen erstem Fall "pot n 0")

-- zuerst Nibble-Rekursion ohne Akkumulator
factNib :: Integer -> Integer
factNib 0 = 1                   -- Abbruchbedingung
factNib n = n * factNib (n-1)   -- rekursiver Schritt

-- Hilfsfunktion mit einem Parameter (Akkumulator) mehr
helper :: Integer -> Integer -> Integer
helper 0 acc = acc -- bei Abbruchbedingung wird Akkumulator retourniert
helper n acc =
    let
        nNeu = n - 1      -- anknabbern des restlichen Inputs
        accNeu = n * acc  -- Ergebnis wird in Akkumulator berechnet
    in
        helper nNeu accNeu

-- nun Tail-Rekursion, dank helper-Funktion
factAcc :: Integer -> Integer
factAcc n = helper n 1      -- 1 ist "Startwert" des Akkumulators
                            -- (Multiplikation mit 1 aendert Ergebnis nicht)

-- alles in einer Funktion
fact :: Integer -> Integer
fact 0 = 1
fact n = accFact n 1
    where
        accFact 0 acc = acc
        accFact n acc = accFact (n-1) (n*acc)   -- in einer Zeile möglich, ohne Hilfsvariable


crossSum :: Integer -> Integer
crossSum 0 = 0
crossSum n = (n `mod` 10) + crossSum (n `div` 10)
{-
Beispiel für Rekursion:
crossSum 123 = 3 + crossSum 12    -- weil: 123 `mod` 10 = 3; 123 `div` 10 = 12
crossSum  12 = 2 + crossSum  1    -- weil:  12 `mod` 10 = 2;  12 `div` 10 =  1
crossSum   1 = 1 + crossSum  0    -- weil:   1 `mod` 10 = 1;   1 `div` 10 =  0
crossSum   0 = 0                  -- weil: erstes Pattern greift

Eingesetzt also:
crossSum 123 = 3 + (crossSum 12) = 3 + (2 + (crossSum 1)) = 3 + (2 + (1 + (crossSum 0))) = 3 + (2 + (1 + 0)) = 6
-}


reverseInt :: Integer -> Integer
reverseInt n = revHelper n 0
    where
        revHelper 0 acc = acc
        revHelper m acc =
            let
                mNeu = m `div` 10              -- Wert um Zehnerstelle nach rechts
                accNeu = acc*10 + (m `mod` 10) -- Wert im Akkumulator eine Stelle nach links + aktuelle Einserstelle
            in
                revHelper mNeu accNeu
{-
Beispiel für Rekursion:
revHelper 123   0 = revHelper 12   3  -- weil: mNeu = 123 `div` 10 = 12; accNeu =  0*10 + (123 `mod` 10) =   3
revHelper  12   3 = revHelper  1  32  -- weil: mNeu =  12 `div` 10 =  1; accNeu =  3*10 + ( 12 `mod` 10) =  32
revHelper   1  32 = revHelper  0 321  -- weil: mNeu =   1 `div` 10 =  0; accNeu = 32*10 + (  1 `mod` 10) = 321
revHelper   0 321 = 321               -- weil: erstes Pattern greift
-}


-- Variante 1
calcPiEuler :: Int -> Float
calcPiEuler steps = sqrt (8 * piAcc steps 0 1)
    where
        piAcc 0 acc cnt = acc
        piAcc n acc cnt = piAcc (n - 1) (acc + (1 / (cnt*cnt))) (cnt + 2)
{-
Hilfsfunktion piAcc hat drei Parameter:
    1)   n ... Anzahl der Schritte, die noch zu berechnen sind
    2) acc ... hier wird die pi-Zahl berechnet
    3) cnt ... für den Nenner des Bruchs, (1/1²+1/3²+1/5²+1/7²+1/9²...)
               deshalb im Rekursionsschritt (cnt+2)
-}

-- Variante zwei: Nibble-Rekursion
calcPiEuler2 :: Int -> Float
calcPiEuler2 steps = sqrt (8 * calcPi steps)
    where
        calcPi 0 = 0
        calcPi n =
            let
                term = 2*(fromIntegral n) - 1
            in
                1/(term*term) + calcPi (n - 1)
{-
Hilfsfunktion calcPi hat nur einen Parameter n: Anzahl der Schritte, die noch zu berechnen sind
Nibble-Rekusion in calcPi:
    - term nimmt Schrittzähler und leitet daraus den notwendigen Nenner des Bruchs ab
    - "fromIntegral" wandelt Int -> Float
In Rekursion dann:
calcPi 3 = 1/5² + calcPi 2    -- weil 2*(fromIntegral 3) - 1 = 2*3 - 1 = 5
calcPi 2 = 1/3² + calcPi 1    -- weil 2*(fromIntegral 2) - 1 = 2*2 - 1 = 3
calcPi 1 = 1/1² + calcPi 0    -- weil 2*(fromIntegral 1) - 1 = 2*1 - 1 = 1
calcPi 0 = 0                  -- weil: erstes Pattern greift -> Abbruchbedingung

Eingesetzt:
calcPi 3 = 1/5² + (calcPi 2) = 1/5² + (1/3² + calcPi 1) = 1/5² + (1/3² + (1/1² + calcPi 0)) = 1/5² + 1/3² + 1/1² + 0
-}

calcPiLeibnitz :: Int -> Float
calcPiLeibnitz steps = 4 * piAcc steps 1 3 (-1)
    where
        piAcc 0 acc arg sign = acc
        piAcc n acc arg sign =
            let
                nNeu = n - 1
                accNeu = acc + (sign / arg) -- Brüche addieren
                argNeu = arg + 2            -- Nenner erhöht sich immer um 2
                signNeu = sign * (-1)       -- Vorzeichen wechselt mit jedem Rekursions-Schritt
            in
                piAcc nNeu accNeu argNeu signNeu
