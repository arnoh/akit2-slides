data NamedContainer a = NC String a
                        deriving (Show, Eq)

instance Functor NamedContainer where
    fmap f (NC str x) = NC str (f x)

instance Applicative NamedContainer where
    pure a = NC "" a
    (NC _ f) <*> (NC str x) = NC str (f x)

-- Beispiel-Aufrufe:
-- pure (+) <*> NC "a" 1 <*> NC "b" 2             == NC "b" 3
-- pure (++) <*> NC "a" "Hallo" <*> NC "b" "Welt" == NC "b" "HalloWelt"
-- fmap (*) (NC "Frage" 21) <*> NC "Antwort" 2    == NC "Antwort" 42


-- Übung 1

data Pair a = P a a
              deriving (Show, Eq)

instance Functor Pair where
    fmap f (P x y) = P (f x) (f y)

-- Bei Pair sind *beide* Bestandteile vom Typ a, nach <*> müssen
-- beide Bestandteile vom Typ b sein.

instance Applicative Pair where
    pure a = P a a
    (P fa fb) <*> (P a b) = P (fa a) (fb b)

-- Beispiel-Aufrufe:
-- pure compare <*> P 1 4 <*> P 0 6 == P GT LT
-- pure (*) <*> P 1 4 <*> P 0 6     == P 0 24


-- Übung 2

data MyList a = Empty | Cons a (MyList a)
                deriving (Show, Eq)

instance Functor MyList where
    fmap f Empty       = Empty
    fmap f (Cons x xs) = Cons (f x) (fmap f xs)

instance Applicative MyList where
    pure a = Cons a Empty
    (Cons f xsf) <*> (Cons a xs) = Cons (f a) (xsf <*> xs)
    _ <*> _ = Empty

-- Beispiel-Aufrufe:
-- fmap (*) (Cons 1 (Cons 2 (Cons 3 Empty))) <*> Empty                   == Empty
-- fmap (-) (Cons 6 (Cons 4 (Cons 3 Empty))) <*> (Cons 3 (Cons 2 Empty)) == Cons 3 (Cons 2 Empty)


-- Übung 3

data Tree a = Leaf | Node (Tree a) a (Tree a)
              deriving (Show, Eq)

instance Functor Tree where
    fmap f Leaf         = Leaf
    fmap f (Node l v r) = Node (fmap f l) (f v) (fmap f r)

instance Applicative Tree where
    pure a = Node Leaf a Leaf
    (Node fl f fr) <*> (Node vl v vr) = Node (fl <*> vl) (f v) (fr <*> vr)
    _ <*> _ = Leaf

-- Beispiel-Aufrufe:
-- fmap (*) (Node (Node Leaf 99 Leaf) 2 (Node Leaf 3 Leaf)) <*> (Node Leaf 2 (Node Leaf 4 Leaf)) == Node Leaf 4 (Node Leaf 12 Leaf)
