-- Aus den Folien

-- Unsicher für negative Zahlen, probieren Sie es aus
wurzel :: Float -> Float
wurzel x = sqrt x

-- eigner Datentyp
data Ergebnis = Zahl Float | Fehler
                deriving (Show)

-- sichere Funktion, da negative Zahlen den Wert "Fehler" retour liefern
wurzelErg :: Float -> Ergebnis
wurzelErg x =
  if x >= 0
    then Zahl (sqrt x)
    else Fehler

-- bereits in Standard-Bibliothek definiert
-- data Maybe a = Nothing | Just a

wurzelM :: Float -> Maybe Float
wurzelM x =
  if x >= 0
    then Just (sqrt x)
    else Nothing

safeHead :: [a] -> Maybe a
safeHead (x:xs) = Just x
safeHead []     = Nothing

-- bereits in Standard-Bibliothek definiert
-- data Either a b = Left a | Right b

wurzelE :: Float -> Either String Float
wurzelE x =
  if x >= 0
    then Right (sqrt x)
    else Left "Zahl kleiner Null!"

safeHeadE :: [a] -> Either String a
safeHeadE (x:xs) = Right x
safeHeadE []     = Left "Leere Liste!"

-- Verwendung

siegerPunkte :: [Int] -> String
siegerPunkte xs =
  case safeHead xs of
    (Just p) -> "Gewinner hat "++(show p)++" Punkte"
    Nothing  -> "Es gab keine Gewinner"

sumWurzelE :: Float -> Float -> Either String Float
sumWurzelE x y =
  case wurzelE x of
    (Left err) -> Left err
    (Right x2) ->
      case wurzelE y of
        (Left err) -> Left err
        (Right y2) -> Right (x2 + y2)

-- Übung

-- 7.2.a
-- analog zu wurzelM
logM :: Float -> Maybe Float
logM x =
    if x <= 0
        then Nothing
        else Just (log x)

-- 7.2.b
-- analog zu wurzelE
logE :: Float -> Either String Float
logE x =
    if x <= 0
        then Left "Log-Berechnung nicht möglich"
        else Right (log x)

-- 7.3.a
wurzelLogM :: Float -> Maybe Float
wurzelLogM x =
    case logM x of
        Nothing -> Nothing
        (Just y) -> wurzelM y
-- da "wurzelM y" bereits vom Typ "Maybe Float" ist, können
-- wir direkt das Ergebnis dieser Berechnung retournieren.

-- 7.3.b
wurzelLogE :: Float -> Either String Float
wurzelLogE x =
    case logE x of
        (Left err) -> Left err
        (Right y) -> wurzelE y
-- auch hier ist "wurzelE y" bereits vom passenden Typ "Either String Float"

-- 7.4.a
-- Pattern-Matching, um Parameter aufzubrechen und beide Fälle zu behandeln
incM :: Maybe Int -> Maybe Int
incM (Just x) = Just (x+1)
incM Nothing   = Nothing

-- 7.4.b
dblM :: Maybe Int -> Maybe Int
dblM (Just x) = Just (x*2)
dblM Nothing   = Nothing

-- 7.4.c
calcM :: (Int -> Int) -> Maybe Int -> Maybe Int
calcM f (Just x) = Just (f x)
calcM f Nothing   = Nothing
-- Datentypen
-- f :: Int -> Int       # die Funktion als Parameter übergeben, z.B. Addition
-- x :: Int
-- f x :: Int
-- Just (f x) :: Maybe Int
-- Nothing :: Maybe Int  # Kommt nichts rein, geht nichts raus

-- 7.4.d
-- mit anonymen Funktionen werden nun Einzeiler daraus
incM2 x = calcM (\y -> y+1) x
dblM2 x = calcM (\y -> y*2) x


-- 7.5.a
incE :: Either String Int -> Either String Int
incE (Right x) = Right (x+1)
incE leftStr   = leftStr
-- leftStr, wenn mit Pattern-Matching aufgebrochen, wäre "Left str"
-- mit "str" :: String. Da wir aber nicht hineinschauen müssen,
-- belassen wir den Parameter in einer Variable "leftStr".

-- 7.5.b
dblE :: Either String Int -> Either String Int
dblE (Right x) = Right (x*2)
dblE (Left s)  = Left s
-- so sähe das Pattern-Matching aus, wenn wir den Left-Fall auch
-- auspacken würden. Danach müssen wir den Wert wieder mit "Left s" herstellen.
-- Im Vergleich zu 7.5.a ist also etwas mehr Arbeit notwendig.
-- da wir "s" aber nicht ändern, ist diese Arbeit nicht notwendig.

-- 7.5.c
--calcE :: (Int -> Int) -> Either String Int -> Either String Int
calcE :: (a -> b) -> Either c a -> Either c b
calcE f (Right x)  = Right (f x)
calcE f (Left str) = Left str
-- Wenn die Signaturen auf Parametervariablen abgeändert werden,
-- ist das "unveränderte" Ein- und Auspacken im Left-Fall zwingend
-- notwendig. Der Grund liegt in der Funktionssignatur: obwohl beide
-- Werte gleich aussehen, ist der Pattern-Matching-Wert vom Typ
-- "Either c a", der neu eingepackte Ergebniswert vom Typ "Either c b"
--
-- Funktionen die möglich sind, die vorher nicht möglich waren:
-- für (a->a) Funktion:
-- calcE sqrt (Right 3.4)  bzw.  calcE (\x -> x+1) (Left True)
-- (weil statt Int Float möglich ist bzw. statt String Bool möglich ist)
--
-- für (a->b) Funktion:
-- calcE odd (Right 3)
-- (weil Int im Right-Fall in Bool umgewandelt wird. D.h. die Signatur
-- "a -> b" sagt, dass wir den Datentyp von "a" auf "b" verändern können.

-- 7.5.d
incE2 x = calcE (\y -> y+1) x
dblE2 x = calcE (\y -> y*2) x
