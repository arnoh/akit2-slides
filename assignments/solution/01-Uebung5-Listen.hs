-- Übung 5.1 Implemetieren Sie die init Funktion, welche alle Elemente bis auf das letzte einer Liste liefert.

init' :: [Int] -> [Int]
init' []     = error "Leere Liste"
init' (x:[]) = []
init' (x:xs) = x : init' xs

-- Übung 5.2 Implementieren Sie die last Funktion, welche das letzte Element einer Liste zurückliefert.

last' :: [Int] -> Int
last' []     = error "Leere Liste"
last' (x:[]) = x
last' (x:xs) = last' xs

-- Übung 5.3 Implementieren Sie die reverse Funktion, welche die Reihenfolge der Elemente in einer Liste umkehrt.

reverse' :: [Char] -> [Char]
reverse' xs = rev xs []
    where
        rev []     acc = acc
        rev (x:xs) acc = rev xs (x:acc)

-- Übung 5.4 Implementieren Sie eine Funktion range', welche zwei Int Parameter engegennimmt.
-- Die Funktion soll eine Liste ([Int]) von aufsteigenden Zahlen liefern, wobei der erste Parameter
-- die Anzahl der Elemente und der zweite Parameter den Startwert (z.B. 0) angibt.

-- Zu verwendende Funktions-Signatur:
range :: Int -> Int -> [Int]
range 0 start = []
range n start = start : range (n-1) (start+1)
