pythagoras :: Int -> Int -> Int -> Bool
pythagoras a b c = a*a + b*b == c*c

-- Klammern in Signatur sind nur zur Hilfe (und nicht zwingend notwendig)
pythagoras1 :: Int -> (Int -> Int -> Bool)
pythagoras1 a = \b c -> a*a + b*b == c*c

-- oder nur noch mit anonymen Funktionen
pythagoras2 :: Int -> Int -> Int -> Bool
pythagoras2 = \a -> (\b -> (\c -> a*a + b*b == c*c))

-- Übung 1

-- Original
applyTwiceToList :: (a -> a) -> [a] -> [a]
applyTwiceToList f xs = map (\x -> f (f x)) xs
-- Umgeschrieben
applyTwiceToList2 :: (a -> a) -> [a] -> [a]
applyTwiceToList2 = \f -> (\xs -> map (\x -> f (f x)) xs)

-- Original
mapFilter :: (a -> b) -> (b -> Bool) -> [a] -> [b]
mapFilter f = \p xs -> filter p (map f xs)
-- Umgeschrieben
mapFilter2 :: (a -> b) -> (b -> Bool) -> [a] -> [b]
mapFilter2 = \f -> (\p -> (\xs -> filter p (map f xs)))


-- Übung 2

-- mapFilter :: (a -> b) -> (b -> Bool) -> [a] -> [b]
-- odd :: Integral a => a -> Bool
-- not :: Bool -> Bool
-- mapFilter odd :: Integral a => (b -> Bool) -> [a] -> [b]
reallyOdd :: Integral a => [a] -> [Bool]
reallyOdd = mapFilter odd not

-- applyTwiceToList :: (a -> a) -> [a] -> [a]
-- zwar beide Parameter angegeben, aber beide sind Parameter der Funktion twice, deshalb:
twice :: (a -> a) -> [a] -> [a]
twice f = \xs -> applyTwiceToList f xs

-- mapFilter :: (a -> b) -> (b -> Bool) -> [a] -> [b]
-- (+1) :: Num a => a -> a
incFilter :: Num a => (a -> Bool) -> [a] -> [a]
incFilter  = mapFilter (+1)

-- pythagoras :: Int -> Int -> Int -> Bool
-- pythagoras x x :: Int -> Bool
-- x is aber Parameter von pytha2, daher:
pytha2 :: Int -> Int -> Bool
pytha2 x   = pythagoras x x

-- incFilter :: Num a => (a -> Bool) -> [a] -> [a]
-- pytha2 :: Int -> Int -> Bool
-- pytha2 3 :: Int -> Bool
-- wird als erster Parameter von incFilter verwendet, daher
whoaFilter :: [Int] -> [Int]
whoaFilter = incFilter (pytha2 3)


-- Übung 3

sumOdd :: [Int] -> Int
sumOdd xs = sum (filter odd xs)

sumOdd2 :: [Int] -> Int
sumOdd2 = sum . filter odd

countW :: Char -> String -> Int
countW ch str =
    length (filter startetMitCh (words str))
    where
        startetMitCh w = head w == ch

countW2 :: Char -> String -> Int
countW2 ch = length . filter startetMitCh . words
  where
    startetMitCh = (== ch) . head

-- Original
mapFilter3 :: (a -> b) -> (b -> Bool) -> [a] -> [b]
mapFilter3 f p xs = filter p (map f xs)
-- mit Komposition
mapFilter4 :: (a -> b) -> (b -> Bool) -> [a] -> [b]
mapFilter4 f p = filter p . map f

-- Original
sumOdd3 :: [Int] -> Int
sumOdd3 xs = foldr (\x y -> x + y) 0 (filter (\z -> odd z) xs)
-- mit Komposition
sumOdd4 :: [Int] -> Int
sumOdd4 = foldr (\x y -> x + y) 0 . filter (\z -> odd z)
-- und weiter vereinfacht
sumOdd5 :: [Int] -> Int
sumOdd5 = foldr (+) 0 . filter odd


-- Weitere Funktion mit Komposition (vgl. Sie mit applyTwiceToList oben)
applyTwiceToList3 :: (a -> a) -> [a] -> [a]
applyTwiceToList3 f = map (f . f)
