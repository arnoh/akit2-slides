-- Funktionen

addTwice :: Int -> Int -> Int
addTwice a b = a + 2*b

-- Funktion, die quadriert
square :: Int -> Int
square x = x * x

-- Operator in Infix-Notation: wie gewöhnlich
addTwo :: Int -> Int -> Int
addTwo x y = x + y

-- Operator in Präfixnotation wird mit Klammern geschrieben
addTwo2 :: Int -> Int -> Int
addTwo2 x y = (+) x y

-- Funktion, die zwei Zahlen quadriert, dann addiert
-- Verwenden Sie addTwo und square; kein let, kein where
sumSquare :: Int -> Int -> Int
sumSquare x y = addTwo (square x) (square y)
-- addTwo nimmt zwei Parameter, deshalb wird "(square x)" und "(square y)" in Klammern gesetzt
-- Falsch: "addTwo square x square y" würde von Haskell als addTwo mit 4 Parametern interpretiert

-- Funktionen in Infix-Notation mit Backtick (verkehrtes Hochkomma)
sumSquare2 :: Int -> Int -> Int
sumSquare2 x y = square x `addTwo` square y


-- Satz von Pythagoras prüfen (a² + b² = c²)
-- Verwenden Sie sumSquare und square
-- Verwenden Sie kein if/case/etc.: der Vergleich hat als Ergebnis schon den passenden Typ
pythagoras :: Int -> Int -> Int -> Bool
pythagoras a b c = sumSquare a b == square c

-- Schreiben Sie sumSquare als Infix-Operator
-- Verwenden Sie addTwo als Infix und square
(+^) :: Int -> Int -> Int
x +^ y = (square x) `addTwo` (square y)
-- in der Signatur ist Schreibweise mit Klammern notwendig
-- den Funktionsbody könnte man auch mit "(+^) x y = ..." einleiten, siehe addTwo2 oben

-- Satz von Pythagoras prüfen (a² + b² = c²)
-- Verwenden Sie (+^) und square
-- Verwenden Sie kein if/case/etc.: der Vergleich hat als Ergebnis schon den passenden Typ
pythagoras2 :: Int -> Int -> Int -> Bool
pythagoras2 a b c = a +^ b == square c
