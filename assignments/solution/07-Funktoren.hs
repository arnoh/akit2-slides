data NamedContainer a = NC String a
                        deriving (Show, Eq)

instance Functor NamedContainer where
    fmap :: (a -> b) -> NamedContainer a -> NamedContainer b
    fmap f (NC str x) = NC str (f x)

-- Typen
--            f :: a -> b
--          str :: String
--            x :: a
--     NC str x :: NamedContainer a
--        (f x) :: b
-- NC str (f x) :: NamedContainer b

-- Beispiel-Aufrufe:
-- fmap (+1) (NC "eins" 1)        == NC "eins" 2
-- fmap length (NC "eins" "zwei") == NC "eins" 4


-- Übung 1

data Tupel c a = T c a
                 deriving (Show, Eq)

-- Funktor-Instanz funktioniert genau gleich wie bei NamedContainer:
-- nur der zweite Teil ist vom Typ a, d.h. nur dort muss die
-- Funktion angewandt werden. Der erste Teil des T-Wertes bleibt unverändert.

instance Functor (Tupel c) where
    fmap :: (a -> b) -> Tupel c a -> Tupel c b
    fmap f (T x y) = T x (f y)

-- Beispiel-Aufrufe:
-- fmap sqrt (T "wurzel" 4)     == T "wurzel" 2
-- fmap length (T False "zwei") == T False 4


-- Übung 2

data Pair a = P a a
              deriving (Show, Eq)

-- Bei Pair sind *beide* Bestandteile vom Typ a, nach fmap müssen
-- beide Bestandteile vom Typ b sein. D.h. die Funktion f muss auf
-- beide Teile angewandt werden.

instance Functor Pair where
    fmap :: (a -> b) -> Pair a -> Pair b
    fmap f (P x y) = P (f x) (f y)

-- Beispiel-Aufrufe:
-- fmap sqrt (P 16 9) == P 4 3
-- fmap length (P "sieben" "zwei") == P 6 4


-- Übung 3

data MyList a = Empty | Cons a (MyList a)
                deriving (Show, Eq)

-- die Instanz für MyList sieht der Implementierung von map für Listen sehr ähnlich
instance Functor MyList where
    fmap :: (a -> b) -> MyList a -> MyList b
    fmap f Empty       = Empty
    fmap f (Cons x xs) = Cons (f x) (fmap f xs)

-- Beispiel-Aufrufe:
-- fmap (*2) Empty == Empty
-- fmap (*2) (Cons 1 (Cons 2 (Cons 3 Empty))) == Cons 2 (Cons 4 (Cons 6 Empty))

-- anhand dieser beiden Funktionen wird die Äquivalenz der beiden Datenstrukturen besonders deutlich
toList :: MyList a -> [a]
toList Empty       = []
toList (Cons x xs) = x : toList xs

fromList :: [a] -> MyList a
fromList []     = Empty
fromList (x:xs) = Cons x (fromList xs)

-- fmap für MyList könnte mit den beiden Funktionen so implentiert werden:
-- fmap f xs = fromList (map f (toList xs))


-- Übung 4

data Tree a = Leaf | Node (Tree a) a (Tree a)
              deriving (Show, Eq)

-- Kern der Implementierung ist, dass fmap sowohl für den linken wie für den rechten
-- Zweig des Baums rekursiv aufgerufen werden muss
instance Functor Tree where
    fmap f Leaf         = Leaf
    fmap f (Node l v r) = Node (fmap f l) (f v) (fmap f r)

-- Beispiel-Aufrufe:
-- fmap odd Leaf == Leaf
-- fmap odd (Node (Node Leaf 8 (Node Leaf 7 Leaf)) 3 (Node (Node Leaf 1 Leaf) 2 Leaf)) ==
--           Node (Node Leaf False (Node Leaf True Leaf)) True (Node (Node Leaf True Leaf) False Leaf)
