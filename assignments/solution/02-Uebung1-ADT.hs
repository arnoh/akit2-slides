-- Übung für algebraische Datentypen und Pattern-Matching

-- 6.1: Angabe der Definitionen

-- "Name" ist Name des Datentyps (Typ-Konstruktor) // "NM" ist Wert des Typs "Name"
data Name    = Nm String
-- "Alter" ist Datentyp (bzw. Typ-Konstruktor) // "Jahre" ist Wert des Typs "Alter"
data Alter   = Jahre Int
-- "Adresse" ist Datentyp (bzw. Typ-Konstruktor) // "Adr" ist Wert des Typs "Adresse"
data Adresse = Adr String
-- "Person" ist Datentyp (bzw. Typ-Konstruktor) // "Pers" ist Wert des Typs "Person"
data Person  = Pers Name Alter Adresse

name :: Name -> String
name (Nm nameStr) = nameStr
-- "name" hat einen Parameter, wir brechen mit Patter-Matching diesen Parameter
-- in seine Bestandteile auf: "Nm" (Daten-Konstruktor/Wert)
-- und "nameStr", der im "Nm"-Wert enthaltene String

alter :: Alter -> Int
alter (Jahre anzahl) = anzahl

pName   :: Person -> String
pName (Pers nameVar _ _) = name nameVar
-- pName nimmt ebenfalls nur einen Parameter, den Wert "Pers".
-- Der enthält drei Attribute vom Typ Name, Alter, Adresse.
-- Wir sind nur am ersten interessiert, also vergeben wir nur hier
-- eine Variable (für die Zuweisung), die anderen beiden Teile
-- sind für die Funktion irrelevant, also benützen wir den Dummy-Namen "_"
-- für diese Parameter
-- Funktionskörper: "nameVar" ist vom Typ "Name", also verwenden wir die
-- vorher definierte Funktion "name" um von Name -> String zu kommen.

pAlter  :: Person -> Int
pAlter (Pers _ alterVar _) = alter alterVar

adresse :: Person -> String
adresse (Pers _ _ (Adr adresseStr)) = addresseStr
-- Wir haben keine Hilfsfunktion für "Adresse", also verwenden wir
-- verschachteltes Pattern-Matching um an den String im Wert "Adr" im Parameter
-- vom Typ "Pers" heranzukommen.

mkPerson  :: String -> Int -> String -> Person
mkPerson nm alt adr = Pers (Nm nm) (Jahre alt) (Adr adr)
-- Die Daten-Konstruktoren (bzw. Werte) "Pers", "Nm", "Jahre", "Adr"
-- der Datentypen "Person", "Name", "Alter" und "Adresse" können
-- wie das Wort "Daten-Konstruktor" nahe legt, zur Erzeugung von Datenwerten
-- verwendet werden. Klammern sind hier wichtig, damit "Pers" drei Argumente
-- sieht (sonst wären es 6 Argumente)
-- Nachteil: Strings können vertauscht werden - mögliche Fehlerquelle bei Aufruf
-- der Funktion

mkPerson2 :: Name -> Alter -> Adresse -> Person
mkPerson2 nm alt adr = Pers nm alt adr
-- Hier können Parameter nicht vertauscht werden


-- Übung 6.2: Definition des Datentyps
data Boolean = Wahr | Falsch
               deriving (Eq, Show)

und   :: Boolean -> Boolean -> Boolean
und Wahr Wahr = Wahr
und _    _    = Falsch
-- mit Pattern-Matching prüfen wir auf die Werte der Parameter
-- wenn beide "Wahr" sind (erstes Pattern), dann ist auch
-- das Ergebnis "Wahr". In allen anderen Fällen ist das Ergebnis
-- "Falsch", wir benötigen also nur ein weiteres Pattern
-- Wir verwenden wieder den Dummy-Namen "_", da wir die Parameter
-- für das Ergebnis nicht brauchen

oder  :: Boolean -> Boolean -> Boolean
oder Falsch Falsch = Falsch
oder _      _      = Wahr
-- Durch geschickte Reihenfolge der Patterns, kommen wir wieder
-- mit zwei Zeilen aus

nicht :: Boolean -> Boolean
nicht Wahr   = Falsch
nicht Falsch = Wahr

xor :: Boolean -> Boolean -> Boolean
xor p q = (p `oder` q) `und` nicht (p `und` q)
-- durch Verwendung der Infix-Notation sieht die Berechnung
-- identisch zur Formel aus

-- Alternative
xor2 :: Boolean -> Boolean -> Boolean
xor2 p q = (p `und` nicht q) `oder` (nicht p `und` q)


-- Übung 6.3

-- Datentyp "Form" besteht aus den drei Werten "Dreieck", "Quadrat" und "Rechteck"
-- Die einzelnen Daten-Konstruktoren nehmen eine unterschiedliche Anzahl an
-- inneren Werten auf
data Form = Dreieck Float Float Float
          | Quadrat Float
          | Rechteck Float Float
          deriving (Show)

-- "umfang" nimmt einen Parameter des Datentyps "Form" auf.
-- D.h. in der Signatur steht *immer* der Datentyp, nie ein Wert (Daten-Konstruktor)
umfang :: Form -> Float
-- Mit Pattern-Matching brechen wir den Parameter in seine Bestandteile auf
-- Wir testen auf alle drei mögliche Werte des Typs "Form"
-- D.h. im Funktionskörper stehen *immer* die Werte/Daten-Konstruktoren, nie
-- der Datentyp/Typ-Konstruktor
umfang (Dreieck a b c) = a + b + c
umfang (Quadrat a)     = 4*a
umfang (Rechteck a b)  = 2*a + 2*b

flaeche :: Form -> Float
flaeche (Quadrat a)     = a*a
flaeche (Rechteck a b)  = a*b
flaeche (Dreieck a b c) = sqrt (s * (s - a) * (s - b) * (s - c))
    where
        s = umfang (Dreieck a b c) -- wir müssen den "Dreieck"-Wert rekonstruieren
                                   -- D.h. wir müssen aus- dann wieder einpacken
