-- Beispiel-Funktionen

safeSqrt :: Float -> Maybe Float
safeSqrt x = if x >= 0
               then Just (sqrt x)
               else Nothing

safeLog :: Float -> Maybe Float
safeLog x = if x > 0
              then Just (log x)
              else Nothing

safeDiv :: Float -> Float -> Maybe Float
safeDiv x 0 = Nothing
safeDiv x y = Just (x / y)

-- Zu Fuss
safeCalc :: Float -> Maybe Float
safeCalc x =
    case safeLog x of
        Nothing   -> Nothing
        (Just x2) ->
            case safeSqrt x2 of
                Nothing   -> Nothing
                (Just x3) -> safeDiv x x3

-- Mit Maybe-Monade und Bind-Operator
safeCalc3 :: Float -> Maybe Float
safeCalc3 x = safeLog x >>= safeSqrt >>= safeDiv x

-- In do-Notation
safeCalc4 :: Float -> Maybe Float
safeCalc4 x = do
    y <- safeLog x
    z <- safeSqrt y
    safeDiv x z

-- Übung 1
-- Gesucht ist die Funktion f(x) = (x/ sqrt(log(x)))/(abs(1 - x))

newCalcZuFuss :: Float -> Maybe Float
newCalcZuFuss x =
    case safeLog x of
        Nothing   -> Nothing
        (Just x2) ->
            case safeSqrt x2 of
                Nothing   -> Nothing
                (Just x3) ->
                    case safeDiv x x3 of
                        Nothing -> Nothing
                        (Just x4) -> safeDiv x4 (abs (1 - x))


newCalcBind :: Float -> Maybe Float
newCalcBind x = safeLog x >>= safeSqrt >>= safeDiv x >>= \y -> safeDiv y (abs (1 - x))

newCalcDo :: Float -> Maybe Float
newCalcDo x = do
    u <- safeLog x
    v <- safeSqrt u
    w <- safeDiv x v
    safeDiv w (abs (1 - x))



-- 2. Protokollierte Berechnung

data Logged a = Log [String] a
                deriving (Eq, Show)

loggedAdd :: (Show a, Num a) => a -> a -> Logged a
loggedAdd x y = Log ["Addition von "++show x++" und "++show y] (x + y)

loggedMul :: (Show a, Num a) => a -> a -> Logged a
loggedMul x y = Log ["Multiplikation von "++show x++" und "++show y] (x * y)

loggedNeg :: (Show a, Num a) => a -> Logged a
loggedNeg x = Log ["Negieren von "++show x] (negate x)


instance Functor Logged where
    -- fmap :: (a -> b) -> Logged a -> Logged b
    fmap f (Log str x) = Log str (f x)


instance Applicative Logged where
    -- pure :: a -> Logged a
    pure x = Log [] x
    -- (<*>) :: Logged (a -> b) -> Logged a -> Logged b
    Log fstr f <*> Log xstr x = Log (fstr ++ xstr) (f x)

appTest1 = (pure id <*> Log ["Start"] 1) == Log ["Start"] 1
appTest2 = (pure (.) <*> Log ["Inc"] (+1) <*> Log ["Mul"] (*2) <*> Log ["Vier"] 4) == (Log ["Inc"] (+1) <*> (Log ["Mul"] (*2) <*> Log ["Vier"] 4))


instance Monad Logged where
    -- return :: a -> Logged a
    return = pure
    -- (>>=) :: Logged a -> (a -> Logged b) -> Logged b
    Log xstr x >>= f =
        let Log str y = f x
        in Log (xstr ++ str) y


monTest1 = (return 7 >>= loggedAdd 3)  ==  loggedAdd 3 7
monTest2 = (loggedAdd 4 5 >>= return)  ==  loggedAdd 4 5
monTest3 = ((loggedNeg 4 >>= loggedMul 3) >>= loggedAdd 12)  ==  (loggedNeg 4 >>= (\x -> loggedMul 3 x >>= loggedAdd 12))


loggedCalcZuFuss :: Int -> Logged Int
loggedCalcZuFuss x =
    let
        Log str1 u = loggedMul 3 x
        Log str2 v = loggedAdd u 4
    in
        Log (str1++str2) v

loggedCalcBind :: Int -> Logged Int
loggedCalcBind x = loggedMul 3 x >>= loggedAdd 4

loggedCalcDo :: Int -> Logged Int
loggedCalcDo x = do
    u <- loggedMul 3 x
    loggedAdd 4 u

-- 2. Funktion

loggedCalc2ZuFuss :: Int -> Int -> Logged Int
loggedCalc2ZuFuss x y =
    let
        Log str1 u = loggedMul 3 x
        Log str2 v = loggedNeg y
        Log str3 w = loggedAdd u v
    in
        Log (str1++str2++str3) w

loggedCalc2Bind :: Int -> Int -> Logged Int
loggedCalc2Bind x y = loggedMul 3 x >>= \u -> loggedNeg y >>= \v -> loggedAdd u v

loggedCalc2Do :: Int -> Int -> Logged Int
loggedCalc2Do x y = do
    u <- loggedMul 3 x
    v <- loggedNeg y
    loggedAdd u v
