% Maybe / Either
% Arno Hollosi; Michael Fladischer

Übungen
=======

In den Folien sind folgende Funktionen definiert worden:

~~~~  { .haskell }
-- unsicher bei negativen Zahlen
wurzel :: Float -> Float
wurzel x = sqrt x

-- eigner Datentyp
data Ergebnis = Zahl Float | Fehler
                deriving (Show)

wurzelErg :: Float -> Ergebnis
wurzelErg x =
  if x >= 0
    then Zahl (sqrt x)
    else Fehler

-- bereits in Standard-Bibliothek definiert
-- data Maybe a = Nothing | Just a

wurzelM :: Float -> Maybe Float
wurzelM x =
  if x >= 0
    then Just (sqrt x)
    else Nothing

safeHead :: [a] -> Maybe a
safeHead (x:xs) = Just x
safeHead []     = Nothing

-- bereits in Standard-Bibliothek definiert
-- data Either a b = Left a | Right b

wurzelE :: Float -> Either String Float
wurzelE x =
  if x >= 0
    then Right (sqrt x)
    else Left "Wurzel-Berechung nicht möglich"

safeHeadE :: [a] -> Either String a
safeHeadE (x:xs) = Right x
safeHeadE []     = Left "Leere Liste!"

-- Verwendung

siegerPunkte :: [Int] -> String
siegerPunkte xs =
  case safeHead xs of
    (Just p) -> "Gewinner hat "++(show p)++" Punkte"
    Nothing  -> "Es gab keine Gewinner"

sumWurzelE :: Float -> Float -> Either String Float
sumWurzelE x y =
  case wurzelE x of
    (Left err) -> Left err
    (Right x2) ->
      case wurzelE y of
        (Left err) -> Left err
        (Right y2) -> Right (x2 + y2)
~~~~

Übung 7.1
---------

1. Kopieren Sie obige Funktionen in eine eigene Datei und laden Sie diese Datei in *ghci*.
2. Führen Sie ein paar Tests aus und vergewissern Sie sich, dass das Ergebnis mit Ihren Erwartungen übereinstimmt. (Negative Zahlen in Klammern schreiben.)
3. Nehmen Sie einige Ihrer Beispielberechnungen und stellen Sie ein `:t` voran, um den Typ abzufragen. Hat der Ausdruck den erwarteten Typ? Hinweis: Zahlen können für mehrere Datentypen stehen, seien Sie als nicht verwundert, wenn Sie anstatt des konkreten Typs `Int` die Typklasse `Num` sehen, oder statt `Float` die Typklasse `Fractional`.

Übung 7.2
---------

**(a)** Schreiben Sie eine Funktion `logM :: Float -> Maybe Float`, welche den natürlichen Logarithmus einer Zahl berechnet, sofern möglich. Hinweis: die Logarithmus-Funktion heißt in Haskell `log`.

**(b)** Schreiben Sie eine Funktion `logE :: Float -> Either String Float`, welche den natürlichen Logarithmus einer Zahl berechnet, sofern möglich. Im Fehlerfall soll der String "Log-Berechnung nicht möglich" retourniert werden.

(Hinweis: orientieren Sie sich als Hilfestellung an `wurzelM` und `wurzelE`.)

Übung 7.3
---------

**(a)** Schreiben Sie mithilfe von `wurzelM` und `logM` die Funktion `wurzelLogM :: Float -> Maybe Float`, welche folgende Berechnung durchführt: $f(x) = \sqrt \log x$
<br>Hinweis: Sie benötigen nur eine `case`-Anweisung. Warum?

**(b)** Analog zu (a), aber als `wurzelLogE :: Float -> Either String Float`. Verwenden Sie die Funktionen `wurzelE` und `logE`.

Wie verhalten sich die beiden Funktionen bei den Zahlen 17, 1.1, 1, 0.5, 0, (-1)?

Übung 7.4
---------

Tipp: Bei nachfolgenden Funktionen können Sie mithilfe von Pattern-Matching auf die `case`-Anweisung verzichten, indem Sie direkt den Funktions-Parameter dem Pattern-Matching unterziehen.

**(a)** Schreiben Sie eine Funktion `incM :: Maybe Int -> Maybe Int`, welche die Int-Zahl (sofern vorhanden) um eins erhöht.

~~~~ { .haskell }
-- Beispiel-Aufrufe
> incM (Just 4)
Just 5
> incM Nothing
Nothing
~~~~

**(b)** Schreiben Sie eine Funktion `dblM :: Maybe Int -> Maybe Int`, welche die Int-Zahl (sofern vorhanden) verdoppelt.

~~~~ { .haskell }
-- Beispiel-Aufrufe
> dblM (Just 4)
Just 8
> dblM Nothing
Nothing
~~~~

**(c)** Die Funktionen `incM` und `dblM` sind sich recht ähnlich. Eine Gelegenheit für Funktionen höherer Ordnung! Schreiben Sie eine neue Funktion `calcM :: (Int -> Int) -> Maybe Int -> Maybe Int`, welche eine Berechnungsfunktion als ersten Parameter übergeben bekommt.

~~~~ { .haskell }
-- Beispiel-Aufrufe
> calcM (\x -> x*x) (Just 4)
Just 16
> calcM negate (Just 4)
Just (-4)
> calcM negate Nothing
Nothing
~~~~

**(d)** Schreiben Sie die Funktionen `incM` und `dblM` neu, diesmal mithilfe von `calcM`.

Übung 7.5
----------

Analog zu Übung 7.4: schreiben Sie die Funktionen `incE :: Either String Int -> Either String Int`, `dblE :: Either String Int -> Either String Int` und `calcE :: (Int -> Int) -> Either String Int -> Either String Int`. Nachdem Sie `calcE` implementiert haben, schreiben Sie `incE` und `dblE` nochmal unter Verwendung von `calcE` neu.

~~~~ { .haskell }
-- Beispiel-Aufrufe
> incE (Right 12)
Right 13
> dblE (Left "Fehlerfall")
Left "Fehlerfall"
> calcE (\x -> x*x) (Right 4)
Right 16
> calcE negate (Right 4)
Right (-4)
> calcE negate (Left "Keine Daten")
Left "Keine Daten"
~~~~


Übung 7.6
---------

(Hinweis: wenn Sie nach dem Umstellen der Signaturen einen Compile-Fehler erhalten, dann haben Sie den Fehler-Zweig der jeweiligen Funktion zwar elegant aber für die Aufgabenstellung unpassend umgesetzt. Packen Sie im Pattern-Matching auch den Fehlerfall zuerst aus, dann wieder ein. Dann sollten Compile-Fehler verschwinden. Falls nicht, posten Sie Ihren Code ins Forum.)

**(a)** Ändern Sie nur die Signaturen von `calcM` und `calcE` wie folgt ab (ohne dass Sie die Implementierung ändern):

~~~~ { .haskell }
calcM :: (a -> a) -> Maybe a -> Maybe a
calcE :: (a -> a) -> Either b a -> Either b a 
~~~~

Warum funktionieren die beiden Funktionen immer noch? Welche Freiheiten haben wir jetzt gewonnen? Finden Sie jeweils einen Beispielaufruf, der nun möglich ist, aber vorher nicht möglich war.

**(b)** Ändern Sie nun die Signaturen von `calcM` und `calcE` wie folgt ab (ohne dass Sie die Implementierung ändern):

~~~~ { .haskell }
calcM :: (a -> b) -> Maybe a -> Maybe b
calcE :: (a -> b) -> Either c a -> Either c b
~~~~

Warum funktionieren die beiden Funktionen immer noch? Welche Freiheiten haben wir jetzt dazugewonnen? Finden Sie jeweils einen Beispielaufruf, der nun möglich ist, aber vorher nicht möglich war.
