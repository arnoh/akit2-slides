% Funktionen
% Arno Hollosi; Michael Fladischer

Definition
----------

In Haskell werden Funktionen wie gewöhnliche Werte definiert, es gibt also kein spezielles
Schlüsselwort *"function"* oder *"method"*. Typischerweise werden Funktionen mit ihrer Signatur
eingeleitet, die Typen der Argumente und des Resultats anzeigt.

~~~~ { .haskell }
addTwice :: Int -> Int -> Int
addTwice a b = a + 2*b
~~~~

Die Funktion `addTwice` nimmt zwei Parameter vom Typ *Int* und liefert ein Ergebnis vom Typ *Int*.

Aufruf
------

Aufgerufen werden Funktionen einfach, indem der Name plus Parameter angeführt werden.
Anders als in anderen Programmiersprachen werden **keine** Klammern verwendet.

~~~~ { .haskell }
addTwice 3 4     -- Name & zwei Parameter
> 11

3 `addTwice` 4   -- Infix-Notation durch Verwendung des umgekehrten Hochkommas "`"
> 11
~~~~

Operatoren können auch in Präfix-Notation verwendet werden:

~~~~ { .haskell }
4 + 5      -- Infix
> 9

(+) 4 5    -- Präfix
> 9
~~~~

Übungen
=======

Übung 1.1
---------

Schreiben Sie eine Funktion `square`, die eine Zahl quadriert (mit sich selbst multipliziert). Geben Sie auch eine passende Funktionssignatur an. Überprüfen Sie das Ergebnis folgender  Beispielaufrufe:

~~~~ { .haskell }
square 3
> 9

square 4
> 16

square (-5)
> 25
~~~~

Prüfen Sie den Typ Ihrer Funktion und eines Ergebnisses mit `:t`

~~~~ { .haskell }
:t square
:t square 3
~~~~

Was passiert, wenn Sie Ihre Funktion mit einem falschen Datentyp aufrufen?

~~~~ { .haskell }
square True
~~~~


Übung 1.2
---------

**(Übung 1.2a)** Schreiben Sie eine Funktion `addTwo`, die zwei Zahlen addiert. Geben Sie auch eine passende Funktionssignatur an. Überprüfen Sie das Ergebnis folgender Beispielaufrufe:

~~~~ { .haskell }
addTwo 3 4
> 7

addTwo (-2) (-5)
> -7
~~~~

**(Übung 1.2b)** Schreiben Sie die Funktion `addTwo2` mit derselben Funktionalität, verwenden Sie aber "+" in Präfix-Notation.


Übung 1.3
---------

**(Übung 1.3a)** Schreiben Sie eine Funktion `sumSquare`, die zwei Zahlen quadriert und dann addiert. Verwenden Sie die zuvor geschriebenen Funktionen `square` und `addTwo`. Geben Sie auch eine passende Funktionssignatur an. Überprüfen Sie das Ergebnis folgender Beispielaufrufe:

~~~~ { .haskell }
sumSquare 3 4
> 25

sumSquare (-3) 2
> 13
~~~~

**(Übung 1.3b)** Schreiben Sie die Funktion `sumSquare2` mit derselben Funktionalität, verwenden Sie aber `addTwo` in Infix-Notation.

Übung 1.4
---------

Schreiben Sie eine Funktion (inkl. Signatur), die den Satz von Pythagoras (a² + b² = c²) prüft.
Verwenden Sie die Funktionen `sumSquare` und `square`.
<br>Hinweis: Verwenden Sie kein if/case/...: der Vergleich mit "`==`" hat als Ergebnis schon den passenden Typ.

~~~~ { .haskell }
pythagoras 3 4 5
> True

pythagoras 1 2 3
> False
~~~~

Übung 1.5
---------

**(Übung 1.5a)** Schreiben Sie Ihren eigenen Operator für das quadrieren & addieren zweier Zahlen: `(+^)`
Das Ergebnis soll identisch mit `sumSquare` sein. Wie müssen Sie die Signatur schreiben?
Können Sie die linke Seite der Funktionsdefinition auf zwei unterschiedliche Arten schreiben?

~~~~ { .haskell }
3 +^ 4
> 25

(+^) (-3) 2
> 13
~~~~

**(Übung 1.5b)** Schreiben Sie die Funktion `pythagoras2`, welche den Infix-Operator `(+^)` anstelle von `sumSquare` verwendet.
