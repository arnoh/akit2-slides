% Komposition & partielle Applikation
% Arno Hollosi; Michael Fladischer

1. Umschreiben
--------------

Eine Funktion mit mehreren Parametern ist eigentlich eine Funktion mit einem Parameter, die eine Funktion retourniert, die einen Parameter weniger hat. Beispiel:

~~~~ { .haskell }
pythagoras :: Int -> Int -> Int -> Bool
pythagoras a b c = a*a + b*b == c*c

-- Klammern in Signatur sind nur zur Hilfe (und nicht zwingend notwendig)
pythagoras1 :: Int -> (Int -> Int -> Bool)
pythagoras1 a = \b c -> a*a + b*b == c*c

-- oder nur noch mit anonymen Funktionen
pythagoras2 :: Int -> Int -> Int -> Bool
pythagoras2 = \a -> (\b -> (\c -> a*a + b*b == c*c))
~~~~

Schreiben Sie folgende Funktionen nach obigem Muster nur noch mit anonymen Funktionen um und vergleichen Sie, ob Sie immer noch dasselbe Resultat liefern:

~~~~ { .haskell }
applyTwiceToList :: (a -> a) -> [a] -> [a]
applyTwiceToList f xs = map (\x -> f (f x)) xs

mapFilter :: (a -> b) -> (b -> Bool) -> [a] -> [b]
mapFilter f = \p xs -> filter p (map f xs)
~~~~


2. Welche Signaturen haben folgende Funktionen?
-----------------------------------------------

Die folgenden Beispiele verwenden bekannte Funktionen bzw. in Übung 1 definierte Funktionen.

~~~~ { .haskell }
reallyOdd  = mapFilter odd not
twice f    = \xs -> applyTwiceToList f xs
incFilter  = mapFilter (+1)
pytha2 x   = pythagoras x x
whoaFilter = incFilter (pytha2 3)
~~~~

Tipp: Sie können in `ghci` überprüfen, ob Sie mit Ihrer Signatur richtig gelegen sind, indem Sie sich mit `:t` die Typen der Beispiele ausgeben lassen.


3. Komposition
--------------

Auf den Folien gab es folgende zwei Beispiele:

~~~~ { .haskell }
sumOdd :: [Int] -> Int
sumOdd xs = sum (filter odd xs)

sumOdd2 :: [Int] -> int
sumOdd = sum . filter odd
~~~~

und

~~~~ { .haskell }
countW :: Char -> String -> Int
countW ch str =
    length (filter startetMitCh (words str))
    where
        startetMitCh w = head w == ch

countW2 :: Char -> String -> Int
countW2 ch = length . filter startetMitCh . words
  where
    startetMitCh = (== ch) . head
~~~~

Schreiben Sie folgende Funktionen mithilfe des Kompositionsoperators um:

~~~~ { .haskell }
mapFilter :: (a -> b) -> (b -> Bool) -> [a] -> [b]
mapFilter f p xs = filter p (map f xs)
-- die neue Funktion soll ohne die Variable "xs" auskommen
~~~~

~~~~ { .haskell }
sumOdd :: [Int] -> Int
sumOdd xs = foldr (\x y -> x + y) 0 (filter (\z -> odd z) xs)
-- die neue Funktion soll ganz ohne Variable auskommen
~~~~
