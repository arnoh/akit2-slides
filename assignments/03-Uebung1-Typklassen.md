% Listen
% Arno Hollosi; Michael Fladischer

Typklassen
----------

Typklassen fassen Typen zu Klassen zusammen. Ein Datentyp kann eine Instanz
einer oder mehrerer Typklassen sein.


Übungen
=======


**Übung 7** Implementieren Sie für den Datentyp `Eintrag` die Typklasse
`Show`, so dass die Ausgabe dem folgenden Beispiel entspricht. Achten Sie auch
darauf, dass in `Eintrag` verwendete Datentypen, falls nötig, ebenso `Show`
implementieren.

~~~~ { .haskell }
-- Zu verwendende Funktions-Signatur:
data Nummer = Nr (Int, Int)
              deriving (Eq)

data Typ = Firma | Person | Service | Geheim

data Eintrag = Etrg Typ String Nummer

show (Etrg  Person "Alice" (Nr (555, 5678))) == "Alice +43/555/5678 (Person)"
~~~~
