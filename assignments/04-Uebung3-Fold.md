% Weitere Übungen für foldl/foldr
% Arno Hollosi; Michael Fladischer

Beispiel: Filtern von Listen
----------------------------

Haskell hat eine Funktion `filter :: (a -> Bool) -> [a] -> [a]`, mit der Elemente aus einer Liste aussortiert werden können: der erste Parameter ist eine Funktion, die für ein Element `True` oder `False` retourniert. Bei `True` bleibt das Element in der Liste, bei `False` wird das Element gelöscht (genauer: nicht in neue Output-Liste mitaufgenommen). Probieren Sie folgende Aufrufe in *ghci* aus, um ein Gefühl für die Funktion zu bekommen:

~~~~ { .haskell }
> filter odd [1..10]
> filter (>7) [1..10]
> filter (\x -> x `mod` 3 == 2) [1..10]
~~~~

Würde man die Funktion selbst und ohne `fold` programmieren, sähe das so aus:

~~~~ { .haskell }
myFilter :: (a -> Bool) -> [a] -> [a]
myFilter f []     = []
myFilter f (x:xs) = if f x
                      then x : myFilter f xs -- x ist dabei
                      else     myFilter f xs -- x ist nicht dabei
~~~~

D.h. wir gehen die Liste rekursiv durch und falls `f x == True`, dann nehmen wir `x` in die neue Liste auf, sonst nicht.

Mit `fold` können wir diese Funktion ohne Rekursion schreiben. Da `foldl` die Reihenfolge der Liste umdreht, benützen wir `foldr`:

~~~~ { .haskell }
myFilter2 :: (a -> Bool) -> [a] -> [a]
myFilter2 f xs = foldr test [] xs
    where
        test x acc = if f x
                       then x : acc -- x ist dabei
                       else     acc -- x ist nicht dabei
~~~~

Das `[]` im `foldr`-Aufruf entspricht unserem ersten Pattern-Matching-Fall bei der vorherigen Lösung. Die Funktion `test` sieht ähnlich wie der zweite Pattern-Matching-Fall aus: statt des rekursiven Aufrufs bauen wir stattdessen den Akkumulator auf.


Übungen
=======


1. delete
---------

Programmieren Sie eine Funktion `delete :: (Eq a) => a -> [a] -> [a]`, welches gleiche Elemente wie den ersten Parameter aus der Liste löscht (mehrfaches Vorkommen möglich):

~~~~ { .haskell }
> delete 3 [1,2,3,4,5,3,7]
[1,2,4,5,7]
> delete 3 []
[]
~~~~

Sie können `delete` in drei Varianten programmieren: (1) mit `foldr`, (2) mit `filter`, (3) rekursiv. Variante (1) sollten Sie jedenfalls umsetzen, die anderen beiden Varianten sind optional.


2. Obstkisten
-------------

Gegeben seien folgende beiden Datentypen:

~~~~ { .haskell }
data Obst = Apfel | Birne | Orange | Banane
            deriving (Show, Eq)

-- eine Kiste hat eine bestimme Anzahl eines Obstes
data Kiste = KisteW { obst  :: Obst
                    , stück :: Int
                    }
                    deriving (Show)
~~~~

Programmieren Sie eine Funktion `summeObst :: Obst -> [Kiste] -> Int`, welches für das angegebene Obst die Gesamt-Stückzahl aller Kisten berechnet. Setzen Sie die Funktion mithilfe von `foldl` um.

**Optional**: Programmieren Sie dieselbe Funktion nur mithilfe der Funktionen `filter`, `map` und `sum`. Wenn Sie Hilfsvariable verwenden, wird es einfacher den Überblick zu behalten.

class Foldable
==============

Da `fold` so ein praktisches und viel verwendetes Werkzeug ist, gibt es in Haskell eine eigene Typklasse `Foldable`, damit man `fold` auch für eigene Datentypen verwenden kann. Die Klasse definiert unter anderem folgende Funktionen:

~~~~ { .haskell }
class Foldable t where
  foldr :: (a -> b -> b) -> b -> t a -> b
  foldl :: (b -> a -> b) -> b -> t a -> b
  foldMap :: Monoid m => (a -> m) -> t a -> m
  foldr1 :: (a -> a -> a) -> t a -> a
  foldl1 :: (a -> a -> a) -> t a -> a
  null :: t a -> Bool
  length :: t a -> Int
  elem :: Eq a => a -> t a -> Bool
  maximum :: Ord a => t a -> a
  minimum :: Ord a => t a -> a
  sum :: Num a => t a -> a
  product :: Num a => t a -> a
~~~~

Die gute Nachricht: damit unser eigener Datentyp eine Instanz der `Foldable`-Klasse erhält, müssen wir nur `foldr` oder `foldMap` umsetzen. Für alle anderen Funktionen gibt es eine Default-Implementierung.

Das folgende Beispiel zeigt eine mögliche Anwendung. Wir wollen eine HashMap implementieren, d.h. eine Datenstruktur mit Key-Value-Paaren. Dafür nehmen wir Tupel: `(String, a)`. Weil die HashMap mehrere Werte speichern können soll, machen wir daraus eine Liste von Tupeln:

~~~~ { .haskell }
data HashMap a = HashMapW [(String, a)]
                 deriving (Show, Eq)
~~~~

Eine mögliche Instanz für `Foldable` sieht dann so aus:

~~~~ { .haskell }
instance Foldable HashMap where
    -- foldr :: (a -> b -> b) -> b -> HashMap a -> b
    foldr f acc (HashMapW []) = acc
    foldr f acc (HashMapW ((key,value):xs)) = f value (foldr f acc (HashMapW xs))
~~~~

Zum Vergleich: hier ist die `foldr`-Instanz für die gewöhnliche Liste:

~~~~ { .haskell }
instance Foldable [] where
    -- foldr :: (a -> b -> b) -> b -> [a] -> b
    foldr f acc [] = acc
    foldr f acc (x:xs) = f x (foldr f acc xs)
~~~~

D.h. die Unterschiede sind: bei der `HashMap`-Instanz müssen wir mittels Pattern-Matching den Wert aufbrechen und der einzelne Wert `x` der Liste ist ein Tupel der Form `(key,value)`. Im rekursiven Schritt müssen wir aus der Tupel-Liste wieder einen vollständigen `HashMapW`-Wert machen, d.h. die Liste mit dem Daten-Konstruktor einpacken.

Beispiel-Aufrufe:

~~~~ { .haskell }
> sum (HashMapW [("Apfel", 10), ("Birne", 20), ("Banane", 5)])
35
> length (HashMapW [("Apfel", 10), ("Birne", 20), ("Banane", 5)])
3
> maximum (HashMapW [("Apfel", 10), ("Birne", 20), ("Banane", 5)])
20
~~~~



Übung 3
-------

Gegeben sei der Datentyp `MyList`, der eine Liste nachbildet. Schreiben Sie für diesen Datentyp eine Foldable-Instanz:

~~~~ { .haskell }
data MyList a = Empty | Cons a (MyList a)
                deriving (Show, Eq)

instance Foldable MyList where
    -- foldr :: (a -> b -> b) -> b -> MyList a -> b
    foldr ...
    -- Alternativ (wenn Sie sich die Monoid-Typklasse bereits angesehen haben)
    -- implementieren Sie *anstatt* foldr die Funktion foldMap:
    -- foldMap :: (Monoid m) => (a -> m) -> MyList a -> m
    -- foldMap ...
~~~~

Nun können Sie `foldl` und `foldr` auch für unseren Datentyp hernehmen:

~~~~ { .haskell }
-- Beispiel-Aufrufe:
> foldl (+) 0 (Cons 1 (Cons 2 (Cons 3 Empty)))
6
> maximum  (Cons 1 (Cons 7 (Cons 3 Empty)))
7
> length (Cons 1 (Cons 2 (Cons 3 Empty)))
3
~~~~

Tipp: sehen Sie sich die Implementierung von `foldr` für gewöhnliche Listen oben an. Dann sollte die Umsetzung für `MyList` keine Probleme bereiten. Falls Sie `foldMap` umsetzen: da das Ergebnis vom Typ `m` ist, der eine Insatz für `Monoid` hat, können Sie auf `mempty` und `mappend` zurückgreifen.


4. Tree
-------

Gegeben sei der Datentyp `Tree`, der einen einfachen, unsortierten Binär-Baum nachbildet. Schreiben Sie für diesen Datentyp eine Foldable-Instanz:

~~~~ { .haskell }
data Tree a = Leaf | Node (Tree a) a (Tree a)
              deriving (Show, Eq)

instance Foldable Tree where
    -- foldr :: (a -> b -> b) -> b -> Tree a -> b
    foldr ...
    -- Alternativ (wenn Sie sich die Monoid-Typklasse bereits angesehen haben)
    -- implementieren Sie *anstatt* foldr die Funktion foldMap:
    -- foldMap :: (Monoid m) => (a -> m) -> Tree a -> m
    -- foldMap ...
~~~~

Tipp: Benützen Sie Hilfsvariablen, sonst wird es schwierig. Bedenken Sie, dass Sie keine Funktion haben, um zwei `b`-Werte zu kombinieren. D.h. Sie müssen vorherige Teil-Ergebnisse als Startwert für die nächste Teil-Berechnung verwenden. Falls Sie `foldMap` umsetzen: nun haben Sie eine Funktion, um zwei Werte zu kombinieren: `mappend`; zudem können Sie auf `mempty` zurückgreifen.

(Hinweis: es gibt mehrere Arten, wie Sie die Teil-Ergebnisse verknüpfen können. `Foldable` schreibt keine bestimmte Implementierung vor, es gibt auch keine Gesetze, die als Richtlinie gelten könnten. Sie können die Teilergebnisse also in jeder beliebigen Reihenfolge verknüpfen.)

~~~~ { .haskell }
-- Beispiel-Aufrufe:
> sum  (Node (Node Leaf 8 (Node Leaf 7 Leaf)) 3 (Node (Node Leaf 1 Leaf) 2 Leaf))
21
> length (Node (Node Leaf 8 (Node Leaf 7 Leaf)) 3 (Node (Node Leaf 1 Leaf) 2 Leaf))
5
> maximum  (Node (Node Leaf 8 (Node Leaf 7 Leaf)) 3 (Node (Node Leaf 1 Leaf) 2 Leaf))
8
~~~~
