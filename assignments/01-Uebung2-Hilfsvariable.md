% Hilfsvariable mit let/in & where
% Arno Hollosi; Michael Fladischer

Hilfsvariable
-------------

Hilfsvariable können auf zwei Arten gebildet werden: mit `let ... in` oder mit `where`.

~~~~ { .haskell }
-- Funktion, die Zahlen quadriert, dann addiert
-- Verwendet addTwo, square und let..in:
letSumSquare :: Int -> Int -> Int
letSumSquare x y =
    let
        x2 = square x
        y2 = square y
    in
        addTwo x2 y2

-- Funktion, die Zahlen quadriert, dann addiert
-- Verwendet addTwo, square und where:
whereSumSquare :: Int -> Int -> Int
whereSumSquare x y = addTwo x2 y2
    where
        x2 = square x
        y2 = square y
~~~~


Übungen
=======

**Übung 2.1**: Schreiben Sie die `pythagoras`-Funktion aus Übung 1.4 mithilfe von `let...in`.

**Übung 2.2**: Schreiben Sie die `pythagoras`-Funktion aus Übung 1.4 mithilfe von `where`.

**Übung 2.3**: Schreiben Sie eine Funktion `fläche3`, die mithilfe des [Satz des Heron](https://de.wikipedia.org/wiki/Satz_des_Heron "Definition auf Wikipedia") die Fläche eines Dreiecks berechnet, wenn die drei Seitenlängen gegeben sind. Die Wurzel-Funktion ist `sqrt`, als Typ sollten Sie `Float` statt `Int` verwenden. Schreiben Sie die Funktion wieder mit Hilfsvariablen (1x mit `let...in`, 1x mit `where`)

~~~~ { .haskell }
fläche3 3 4 5
> 6.0

fläche3 2 4 5
> 3.799671038392666
~~~~
