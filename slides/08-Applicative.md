AKIT2
=====

Funktionale Programmierung mit Haskell
--------------------------------------

Arno Hollosi & Michael Fladischer

SS 2016, FH CAMPUS 02


== Slide ==

Applicative
===========


Motivation: Partielle Applikation für Functoren
-----------------------------------------------

```haskell
Prelude> let x = fmap (+) Just 2
-- Type nach erstem fmap
Prelude> :t x
fmap (+) (Just 2) :: Num a => Maybe (a -> a)
-- Zweiten Parameter mappen
Prelude> fmap x (Just 2)
```

Ziel: Funktionen mit zwei oder mehr Parametern in einem Functor verwenden
<!-- .element class="fragment" -->


Beispiele
---------

Partielle Applikation von Funktionen mit zwei Parametern
```haskell
Prelude> :t fmap (++) (Just "hey")
fmap (++) (Just "hey") :: Maybe ([Char] -> [Char])
Prelude> :t fmap compare (Just 'z')
fmap compare (Just 'a') :: Maybe (Char -> Ordering)
Prelude> :t fmap compare "A LIST OF CHARS"
fmap compare "A LIST OF CHARS" :: [Char -> Ordering]
```


Definition
----------

```haskell
class (Functor f) => Applicative f where
  pure  :: a -> f a
  (<*>) :: f (a -> b) -> f a -> f b
```

Ableitungen:
* `Applicative` setzt `Functor` voraus!
* Gibt zwei Funktionen, `pure` und `<*>`, vor.


Beispiel: Maybe
-----------------------

`Applicative` Typklasse für `Maybe`

```haskell
instance Applicative Maybe where
  pure                  = Just
  (Just f) <*> (Just x) = Just (f x)
  _        <*> _        = Nothing
```
<!-- .element class="fragment" -->


Beispiel: Maybe
-----------------------

```haskell
ghci> Just (+2) <*> Just 3
Just 5
ghci> pure (+2) <*> Just 10
Just 12
ghci> pure (+2) <*> Just 9
Just 11
ghci> Just (++"Hallo") <*> Nothing
Nothing
ghci> Nothing <*> Just "Welt"
Nothing
```


Vergleich: Functor
------------------

```haskell
-- Functor
Prelude> :t fmap
fmap :: Functor f => (a -> b) -> f a -> f b
-- Applicative
Prelude> :t (<*>)
(<*>) :: Applicative f => f (a -> b) -> f a -> f b
```
