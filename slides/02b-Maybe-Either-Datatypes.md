AKIT2
=====

Funktionale Programmierung mit Haskell
--------------------------------------

Arno Hollosi & Michael Fladischer

SS 2016, FH CAMPUS 02


== Slide ==

Maybe / Either
==============

Wichtige Datentypen zur Fehlerbehandlung


== Slide ==

Wiederholung: Definition
------------------------

```haskell
data Fahrrad = Rad {
  gänge :: Int,
  farbe :: String,
  marke :: String
}
```

oder

```haskell
data Fahrrad = Rad Int String String
```

D.h. `Rad` ist Wert vom Typ `Fahrrad`,<br>ähnlich wie `3.2` ein Wert vom Typ `Float` ist.


Wie Fehler abbilden?
--------------------

```haskell
head :: [a] -> a
head (x:xs) = x
head []     = ??
```

Haskell kennt kein `null`.
Was tun?

`error` ist nicht immer wünschenswert


Idee
----

* Datentypen können mehrere Werte haben, z.B.
  `data Color = Red | Green | Blue`

* Einen Wert definieren für `Ok`
* Einen Wert definieren für `Fehler`


Beispiel: Wurzel
----------------

```haskell
wurzel :: Float -> Float
wurzel x = sqrt x
```

Statt `Float` nun eigener Datentyp:

```haskell
data Ergebnis = Zahl Float | Fehler
```

D.h. wir definieren einen Datentyp `Ergebnis`, der entweder ein Float enthält (im Wert `Zahl`) oder den Wert `Fehler` hat (ohne Float).


Neue Wurzel-Funktion
--------------------

```haskell
data Ergebnis = Zahl Float | Fehler
                deriving (Show)

wurzel :: Float -> Ergebnis
wurzel x = if x >= 0
             then Zahl (sqrt x)
             else Fehler
```

* Funktions hat nun Resultat vom Typ `Ergebnis`
* x>=0: Ergebnis wird in den `Zahl`-Wert "eingepackt"
* Sonst: retournieren des `Fehler`-Werts


Beispielaufrufe
---------------

```haskell
> wurzel 4
Zahl 2.0
> wurzel 2
Zahl 1.4142135
> wurzel (-2)
Fehler
```


Allgemeiner möglich?
--------------------

Unser `Ergebnis`-Datentyp kann nur `Float`-Ergebnisse beinhalten.

```haskell
data Ergebnis = Zahl Float | Fehler
```

Wie können wir das verallgemeinern?

--> statt `Float` eine Typvariable verwenden!


== Slide ==

Definition Maybe
================

```haskell
data Maybe a = Nothing | Just a
```

* Typkonstruktur `Maybe`
* Kann jeden anderen Typ beinhalten<br>
  (deshalb die Typvariable `a`)
* Erster Wert für Fehlerfall: `Nothing`
* Zweiter Wert für Ergebnis: `Just a`


Ist Maybe ein Datentyp?
-----------------------

`Maybe` für sich alleine ist kein Datentyp!

Es ist ein **Typ-Konstruktor**, um andere Datentypen zu erzeugen.

* `Maybe Int` ist ein Datentyp (z.B. mit Wert `Just 3`)
* `Maybe String` ist einer (z.B. mit `Just "fun"`)
* `Maybe Float` ist einer (z.B. mit `Just 3.4`)
* etc.

... aber `Maybe` für sich alleine ist kein Datentyp sondern ein Typ-Konstruktor.


Beispiel: wurzel
----------------

```haskell
wurzel :: Float -> Maybe Float
wurzel x = if x >= 0
             then Just (sqrt x)
             else Nothing
```

Vergleich mit vorher:
* Aus `Ergebnis` wird `Maybe Float`
* Aus `Zahl` wird `Just`
* Aus `Fehler` wird `Nothing`


Beispiel: head
--------------

```haskell
safeHead :: [a] -> Maybe a
safeHead (x:xs) = Just x
safeHead []     = Nothing
```

`safeHead` nimmt eine Liste jeden beliebigen Typs `a` und liefert möglicherweise ein Ergebnis zurück, deshalb `Maybe a`


Beispiel: head mit Int
----------------------

```haskell
-- so sähe Signatur mit Int statt mit "a" aus
-- safeHead :: [Int] -> Maybe Int
-- Beispielaufrufe
> let xs = [1,2,3] :: [Int]
> safeHead xs
Just 1
> :t safeHead xs
safeHead xs :: Maybe Int
> safeHead []
Nothing
```

Verwendung von Typvariable `a` erlaubt es uns, jeden Datentyp "einzusetzen" bzw. zu verwenden.


Warum ist Just notwendig?
-------------------------

```haskell
safeHead :: [a] -> Maybe a
safeHead (x:xs) = Just x
safeHead []     = Nothing
```

Die Funktion muss einen Wert vom Typ `Maybe a` retournieren.
`x` ist aber nicht von diesem Datentyp, sondern nur vom Datentyp `a`.
--> Wir müssen `x` "umwandeln"

`Just` packt uns den Wert `x` in einen Wert des Typs `Maybe a` ein.


Datenkonstruktoren als Funktion
-------------------------------

```haskell
> :t Just
Just :: a -> Maybe a
> :t Nothing
Nothing :: Maybe a
```

D.h. Datenkonstruktoren können als Funktion verstanden (und behandelt) werden,
die Werte des zugehörigen Datentyps erzeugen (bzw. selbst darstellen).

Z.B. `Just` erzeugt (bzw. ist) Wert von `Maybe a`,
<br>`Rad` erzeugt (bzw. ist) Wert von `Fahrrad`,
<br>`Blue` erzeugt (bzw. ist) Wert des Typs `Color`.


== Slide ==

Either
======

Wenn wir auch eine Fehlermeldung retournieren wollen.

(`Maybe` gibt bei `Nothing` ja keinen zusätzlichen Wert an.)


Definition
----------

```haskell
data Either a b = Left a | Right b
```

* `Either` ist wieder Typ-Konstruktor
* Zwei Typvariablen in Definition!
* `a` für den Fehlerfall in `Left a`
* `b` für den Ok-Fall in `Right b`


Beispiel: wurzel
----------------

```haskell
wurzel :: Float -> Either String Float
wurzel x = if x >= 0
             then Right (sqrt x)
             else Left "Zahl kleiner Null!"
```

```haskell
-- Beispielaufrufe
> wurzel 20
Right 4.472136
> wurzel (-20)
Left "Zahl kleiner Null!"
```


wurzel im Detail
----------------

Ergebnis ist vom Typ `Either String Float`
* `Either` - Typ-Konstruktor
* `String` - Datentyp für Fehlerfall
* `Float` - Datentpy für Ok-Fall

```haskell
wurzel :: Float -> Either String Float
wurzel x =
  if x >= 0
  -- Float-Wert "sqrt x" wird in Right-Wert verwendet
     then Right (sqrt x)
  -- String-Wert wird in Left-Wert (Fehlerfall) verwendet
     else Left "Zahl kleiner Null!"
```


Beispiel: head
--------------

```haskell
safeHead :: [a] -> Either String a
safeHead (x:xs) = Right x
safeHead []     = Left "Leere Liste!"
```

`safeHead` nimmt eine Liste jeden beliebigen Typs `a` und liefert ein Ergebnis oder einen Fehler-String zurück, deshalb `Either String a`


== Slide ==

Verwendung
==========

Maybe & Either verwenden

Wie kommen wir an innere Werte ran?


Beispiel: Punkteliste
---------------------

```haskell
siegerPunkte :: [Int] -> String
siegerPunkte xs =
  let
    x = safeHead xs -- x :: Maybe Int
  in
    case x of
      (Just p) -> "Gewinner hat "++(show p)++" Punkte"
      Nothing  -> "Es gab keine Gewinner"
```

Wir verwenden Pattern-Matching, um an den inneren Wert in `Just p` heranzukommen.

(`show`-Funktion erzeugt String, ähnlich wie toString() in Java)


Kürzer: Punkteliste
-------------------

```haskell
-- ohne Hilfsvariable
siegerPunkte :: [Int] -> String
siegerPunkte xs =
  case safeHead xs of
    (Just p) -> "Gewinner hat "++(show p)++" Punkte"
    Nothing  -> "Es gab keine Gewinner"
```

Der Datentyp "zwingt" uns also dazu, auch `Nothing` zu berücksichtigen.
Eine NullPointerException wie z.B. in Java kann es daher nicht geben, da wir
immer beide Fälle prüfen müssen.


Beispiel: Summe zweier Wurzeln
------------------------------

```haskell
sumWurzel :: Float -> Float -> Either String Float
sumWurzel x y =
  case wurzel x of
    (Left err) -> Left err
    (Right x2) ->
      case wurzel y of
        (Left err) -> Left err
        (Right y2) -> Right (x2 + y2)
```

Wir verschachteln die Abfragen und liefern nur dann ein Ergebnis zurück wenn beides `Right`-Werte sind.

(Wir werden noch später sehen, wie wir dieses Verschachteln verhindern können.)
