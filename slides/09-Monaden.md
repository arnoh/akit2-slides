AKIT2
=====

Funktionale Programmierung mit Haskell
--------------------------------------

Arno Hollosi & Michael Fladischer

SS 2016, FH CAMPUS 02


== Slide ==

Monaden
=======


Motivation
----------

$f(x) = \sqrt \log x$

Definitionsbereich:
<!-- .element class="fragment" -->

* $\sqrt x \ldots \forall x \geq 0$
<!-- .element class="fragment" -->
* $\log x \ldots \forall x \gt 0$
<!-- .element class="fragment" -->


Haskell-Funktionen
------------------

```haskell
safeSqrt :: Float -> Maybe Float
safeSqrt x = if x >= 0
               then Just (sqrt x)
               else Nothing
```

```haskell
safeLog :: Float -> Maybe Float
safeLog x = if x > 0
              then Just (log x)
              else Nothing
```

`safeSqrt (safeLog x) ??`
<!-- .element class="fragment" -->


"Zu Fuß"
--------

```haskell
safeCalc :: Float -> Maybe Float
safeCalc x =
    case safeLog x of
        Nothing   -> Nothing
        (Just x2) -> safeSqrt x2
```

```haskell
safeLog :: Float -> Maybe Float
safeSqrt :: Float -> Maybe Float
```


Allgemeine Bindung
------------------

```haskell
-- Ausgangslage
x  :: c
f1 :: c -> m a  -- z.B. safeLog
f2 :: a -> m b  -- z.B. safeSqrt
```
<!-- .element class="fragment" -->

```haskell
-- Gesucht
bind :: m a -> (a -> m b) -> m b
```
<!-- .element class="fragment" -->

```haskell
-- Dann ginge:
(f1 x) `bind` f2
```
<!-- .element class="fragment" -->


Bind für Maybe
--------------

```haskell
bindMaybe :: Maybe a -> (a -> Maybe b) -> Maybe b
bindMaybe ma f =
    case ma of
       Nothing  -> Nothing
        (Just x) -> f x
```

```haskell
-- Beispiel:
safeCalc :: Float -> Maybe Float
safeCalc x = safeLog x `bindMaybe` safeSqrt
```
<!-- .element class="fragment" -->


Bind für Either
---------------

```haskell
bindEither :: Either c a
           -> (a -> Either c b)
           -> Either c b
bindEither eca f =
    case eca of
        (Left l)  -> Left l
        (Right r) -> f r
```


Bind für Liste
---------------

```haskell
bindList :: [a] -> (a -> [b]) -> [b]
bindList []     f = []
bindList (x:xs) f = f x ++ bindList xs f
```

```haskell
-- Beispiel: alle Wörter einer Liste von Strings
-- words :: String -> [String]
allWords :: [String] -> [String]
allWords xs = xs `bindList` words
```


Vergleich
---------

```haskell
-- Signaturen der Bind-Funktionen
Maybe a    -> (a -> Maybe b)    -> Maybe b
Either c a -> (a -> Either c b) -> Either c b
[a]        -> (a -> [b])        -> [b]
```

-> Typklasse!<!-- .element class="fragment" -->


== Slide ==

Monade
======

```haskell
class Applicate m => Monad m where
    return :: a -> m a
    (>>=)  :: m a -> (a -> m b) -> m b
```

* `return`: Packt in Struktur ein
* `bind`: Komposition der Berechnungen
* Jede Monade ist auch ein applikativer Funktor


Beispiel: Parsen
----------------

HTTP-Kopfzeile: `GET /page HTTP/1.1`

Funktionen:

```haskell
-- Alle vom Typ a -> m b
readMethod  :: String -> Maybe (Method, String)
readUrl     :: String -> Maybe (Url, String)
readVersion :: String -> Maybe (Version, String)

-- Gesucht:
readHeader :: String -> Maybe (Method, Url, Version)
```


Manuell: Viel zu mühsam
-----------------------

```haskell
readHeader s =
  case readMethod s of
    Nothing -> Nothing
    Just (method, s2) ->
      case readUrl s2 of
        Nothing -> Nothing
        Just (url, s3) ->
          case readVersion s3 of
            Nothing -> Nothing
            Just (version, _) -> Just (method, url, version)
```


Mit Maybe-Monade
----------------

```haskell
readHeader str =
  readMethod str                      >>=
  \(method, str2) -> readUrl str2     >>=
  \(url, str3)    -> readVersion str3 >>=
  \(version, _)   -> Just (method, url, version)
```

So häufig, dass es eigene Syntax dafür gibt:


Do-Notation
-----------

```haskell
readHeader str = do
  (method, str2) <- readMethod str
  (url,    str3) <- readUrl s2
  (version, _  ) <- readVersion s3
  return (method, url, version)
```


Do-Notation: safeCalc
---------------------

```haskell
safeCalc :: Float -> Maybe Float
safeCalc x = do
    x2 <- safeLog x
    x3 <- safeSqrt x2
    return x3
```

```haskell
-- oder Resultat direkt verwenden
safeCalc :: Float -> Maybe Float
safeCalc x = do
    x2 <- safeLog x
    safeSqrt x2
```


Do-Notation: FileHandling
-------------------------

```haskell
-- Pseudo-Code
copyFile :: FilePath -> FilePath -> IO Result
copyFile from to = do
    file  <- openFile from
    bytes <- readFile file
    file2 <- openFile to
    writeFile file2 bytes
```

Keine If-then-else mehr!


== Slide ==

Regeln für Monaden
------------------

```haskell
class Monad where
  return :: a -> m a
  (>>=) :: m a -> (a -> m b) -> m b
```

```haskell
-- Links-Identität
return a >>= f  ==  f a

-- Rechts-Identität
m >>= return  ==  m

-- Assoziativität
(m >>= f) >>= g  ==  m >>= (\x -> f x >>= g)
```
