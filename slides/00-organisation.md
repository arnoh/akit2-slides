AKIT2
=====

Funktionale Programmierung mit Haskell
--------------------------------------

Arno Hollosi & Michael Fladischer

SS 2016, FH CAMPUS 02


== Slide ==

Inhalte
=======

* Funktionales Programmieren <!-- .element class="fragment" -->
  * Motivation, Vorteile, Paradigma
* Kernkonzepte von Haskell <!-- .element class="fragment" -->
  * Lazy Evaluation und Purity
  * Funktionen höherer Ordnung und Currying
  * Algebraische Datentypen & Typ-Klassen, Polymorphismus
* Verknüpfung von Daten <!-- .element class="fragment" -->
  * Funktor, Monoid
  * Applikativ, Monade
* Testen & Anwendungsbeispiele <!-- .element class="fragment" -->


== Slide ==

Lernziele
=========

* Funktionales Programmier-Paradigma
  * Eigenschaften und Konzepte anwenden
  * Funktionen höherer Ordnung selbst erstellen
* Haskell
  * einfache Programme schreiben und analysieren
  * Eigene Datenstrukturen definieren
* Typisierung
  * Typklassen und Typfamilien passend anwenden
  * algebraische Datenstrukturen verwenden
* Verknüpfungen und Abläufe
  * Funktor, Monoid, Applikativ, Monade: beschreiben, anwenden,
    Abstraktionen bewerten


== Slide ==

Beurteilung
===========

* Immanenter Prüfungscharakter <!-- .element class="fragment" -->
  * Übungen (65%)
  * Endklausur (35%)
* Beide Teile müssen positiv sein <!-- .element class="fragment" -->
* Zweiter und dritter Antritt analog <!-- .element class="fragment" -->
  * Positive Einzelbeurteilungen bleiben aufrecht
  * Dritter Antritt ist kommissionelle Prüfung
