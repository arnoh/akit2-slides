AKIT2
=====

Funktionale Programmierung mit Haskell
--------------------------------------

Arno Hollosi & Michael Fladischer

SS 2016, FH CAMPUS 02


== Slide ==

Funktoren
=========


Motivation: Wiederverwendung
----------------------------

Beispiel für gewöhnliche Funktion:
```haskell
-- MwSt hinzufügen
addVAT :: Float -> Float
addVAT price = price * 1.2
```

Ziel: Funktion ohne Änderung in verschiedenen Kontexten wiederverwenden
<!-- .element class="fragment" -->


Beispiel-Kontext: Liste
-----------------------

```haskell
-- Ziel
listAddVAT :: [Float] -> [Float]
-- vorhanden
addVAT :: Float -> Float
```
<!-- .element class="fragment" -->

Funktion?
<!-- .element class="fragment" -->

```haskell
map :: (a -> b) -> [a] -> [b]

listAddVAT :: [Float] -> [Float]
listAddVAT xs = map addVAT xs
```
<!-- .element class="fragment" -->


Beispiel-Kontext: Maybe
-----------------------

```haskell
-- Ziel
maybeAddVAT :: Maybe Float -> Maybe Float
-- vorhanden
addVAT :: Float -> Float
```
<!-- .element class="fragment" -->

Funktion?
<!-- .element class="fragment" -->

```haskell
mapMaybe :: (a -> b) -> Maybe a -> Maybe b
mapMaybe f Nothing  = Nothing
mapMaybe f (Just x) = Just (f x)

maybeAddVAT :: Maybe Float -> Maybe Float
maybeAddVAT x = mapMaybe addVAT x
```
<!-- .element class="fragment" -->


Beispiel-Kontext: Either
------------------------

```haskell
-- Ziel
eitherAddVAT :: Either c Float -> Either c Float
-- vorhanden
addVAT :: Float -> Float
```
<!-- .element class="fragment" -->

Funktion?
<!-- .element class="fragment" -->

```haskell
mapEither :: (a -> b) -> Either c a -> Either c b
mapEither f (Left l)  = Left l
mapEither f (Right x) = Right (f x)

eitherAddVAT :: Either c Float -> Either c Float
eitherAddVAT x = mapEither addVAT x
```
<!-- .element class="fragment" -->


Vergleich
---------

```haskell
-- Signaturen
listAddVAT   :: [Float]        -> [Float]
maybeAddVAT  :: Maybe Float    -> Maybe Float
eitherAddVAT :: Either c Float -> Either c Float
```
<!-- .element class="fragment" -->

```haskell
-- Hilfsfunktionen
map       :: (a -> b) -> [a]        -> [b]
mapMaybe  :: (a -> b) -> Maybe a    -> Maybe b
mapEither :: (a -> b) -> Either c a -> Either c b
```
<!-- .element class="fragment" -->

```haskell
-- Anwendung
listAddVAT  xs = map       addVAT xs
maybeAddVAT  x = mapMaybe  addVAT x
eitherAddVAT x = mapEither addVAT x
```
<!-- .element class="fragment" -->
