AKIT2
=====

Funktionale Programmierung mit Haskell
--------------------------------------

Arno Hollosi & Michael Fladischer

SS 2016, FH CAMPUS 02


== Slide ==

Typklassen
==========


Motivation: Starke Typisierung
------------------------------

Haskell ist stark & statisch typisiert

* **Stark**: Nur explizite Konvertierung möglich
* **Statisch**: Überprüfung bei Übersetzung


Beispiel: Länge einer Liste von Chars berechnen
-----------------------------------------------

```haskell
lengthC :: [ Char ] -> Int
lengthC []     = 0
lengthC (_:xs) = 1 + lengthC xs
```


Beispiel: Länge einer Liste von Floats berechnen
------------------------------------------------

```haskell
lengthF :: [ Float ] -> Int
lengthF []     = 0
lengthF (_:xs) = 1 + lengthF xs
```


Datentypen & Typvariablen
--------------------

Haskell kennt **Datentypen** ...

 `Char`, `Int`, `Float`, `Bool`, `...`


... und **Typvariablen**:

* sind kleingeschriebene Bezeichner
* häufig verwendet: `a`, `b`, `c`
* stehen für **beliebige Datentypen**


Beispiel: Länge einer Liste berechnen
-------------------------------------

```haskell
-- Vorher, mit konkretem Datentyp Float
lengthF :: [ Float ] -> Int
lengthF []     = 0
lengthF (_:xs) = 1 + lengthF xs
```

```haskell
-- Nachher, mit Typvariable a
length :: [ a ] -> Int
length []     = 0
length (_:xs) = 1 + length xs
```


Datentypen einschränken?
------------------------

Uneingeschränkte Typvariablen funktionieren nicht immer!

```haskell
sumA :: [ a ] -> a
sumA []     = 0
sumA (x:xs) = x + sumA xs
```

Fehlermeldung:
```plain
No instance for (Num a) arising from the literal ‘0’
Possible fix:
	add (Num a) to the context of
		the type signature for sumA :: [a] -> a
In the expression: 0
In an equation for ‘sumA’: sumA [] = 0
```


Was wäre das Ergebnis von
```haskell
sumA [True, False, True]
```
oder
```haskell
sumA "Hallo Welt"
```
**???**


== Slide ==

Typklassen helfen weiter
------------------------

Fassen Typen zu Klassen zusammen.

*(eigentlich umgekehrt: Typklassen mit Datentypen als Instanzen)*


Beispiel: Typklasse für (+)-Operation
-------------------------------------

Typklasse `Num` für Datentypen die *(unter anderem)* die `(+)`-Operation kennen.

```haskell
sumA :: (Num a) => [a] -> a
sumA [] = 0
sumA (x:xs) = x + sumA xs
```

Für `a` können nun nur Datentypen verwendet werden, die zur Typklasse `Num`
gehören.


Typklasse: Eq
-------------

Wird für Gleichheitsprüfung verwendet, Datentypen müssen die Funktionen `==` und
`/=` implementieren.

```haskell
Prelude> 7 == 7
True
Prelude> 9 /= 9
False
```

Datentypen: `Bool`, `Char`, `Int`, `Integer`, `Float`, `Double`, `Rational`, ...


Typklasse: Ord
--------------

Für Datentypen die eine Reihenfolge/Rangordnung haben. Datentyp muss auch
Typklasse `Eq` haben.

```haskell
Prelude> "Alice" `compare` "Bob"
LT
Prelude> 42 `compare` 20
GT
Prelude> 42 `compare` 42
EQ
```

Datentypen: `Bool`, `Char`, `Int`, `Integer`, `Float`, `Double`, `Rational`, ...


Typklasse: Show
---------------

Datentypen dieser Typklasse können als `String` repräsentiert werden. Die meisten
Datentypen sind Mitglied, Funktionen jedoch nicht.

```haskell
show 42 == "42"
show 23.2323 == "23.2323"
show False == "False"
```

Datentypen: `Bool`, `Char`, `Int`, `Integer`, `Float`, `Double`, `Rational`, ...


Typklasse: Num
--------------

Typklasse deren Datentypen sich wie rationale oder ganze Zahlen verhalten können.

```haskell
Prelude> :t 42
42 :: (Num t) => t
Prelude> :t (+)
(+) :: Num a => a -> a -> a
```

Datentypen: `Int`, `Integer`, `Float`, `Double`, `Rational`


Typklasse: Integral
-------------------

Wie `Num` jedoch nur Ganzzahlen.

Datentypen: `Int`, `Integer`


Typklasse: Fractional
---------------------

Wie `Num` jedoch rationale Zahlen.

Datentypen: `Float`, `Double`, `Rational`


Typklasse: Floating
-------------------

Wie `Fractional` jedoch für Gleitkommazahlen.

Datentypen: `Float`, `Double`


Automatische Instanz einer Typklasse
------------------------------------

Funktioniert für `Eq`, `Ord`, `Enum`, `Bounded`, `Show` oder `Read`:
```haskell
-- Datentyp für Telefon-Nummer
data Nummer = Nr (Int, Int)
              deriving (Eq)
Nr (555, 1234) == Nr (555, 1234)
```


Instanz einer Typklasse implementieren
--------------------------------------

```haskell
instance Show Nummer where
  show (Nr (vw, nr)) =
    '0' : show vw ++ "/" ++ show nr

show Nr (555, 1234)
```
