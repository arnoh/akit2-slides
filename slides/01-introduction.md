AKIT2
=====

Funktionale Programmierung mit Haskell
--------------------------------------

Arno Hollosi & Michael Fladischer

SS 2016, FH CAMPUS 02


== Slide ==

Funktionale Programmierung
==========================


Funktionen sind die Kerneinheit
-------------------------------

Funktionen sind first class

--> sind als Werte & Parameter verwendbar


Ohne Seiteneffekte (pur)
-------------------------

Keine Statusvariable (weder lokal noch global)

--> verhält sich wie mathematische Formel


Kombinierbar (composable)
-------------------------

„Algebra of programs“ (Backus, 1977)


Hohes Abstraktionsvermögen
---------------------------

--> hohe Wiederverwendbarkeit


Funktionale Programmierung
--------------------------

* Funktionen
* pur
* kombinierbar
* Abstraktionen bilden


== Slide ==

Funktionale Sprachen
====================


Rein funktional
---------------

* Haskell
* Elm
* Miranda
* Idris
* PureScript


Starker funktionaler Fokus
--------------------------

* Erlang
* ML / SML
* Lisp
* F#
* Ocaml
* Clojure


Mehrere Paradigmen
------------------

* Python
* Ruby
* Scala
* JavaScript
* R
* Java
* C#
* ...


== Slide ==

Haskell
=======


* Benannt nach Haskell B. Curry (1900-1982)
* 1987 entstanden (akademisches Umfeld)

![Haskell B. Curry](images/haskell-curry.jpg "Haskell B. Curry, Quelle: wiki.haskell.org")


Prinzipien
----------

* Funktional
* Pur
* Lazy Evaluation (non-strict)
* Starke, statische Typisierung


== Slide ==

Wholemeal Programming
=====================

Nicht **was**, sondern **wie** steht im Vordergrund.


Imperatives Beispiel
--------------------

```
int acc = 0;
for (int i = 0; i < lst.length; i++) {
      acc = acc + 3 * lst[i];
}
```

Beschreibt Prozess der Berechnung<br> (Schleife etc.) <!-- .element class="fragment" -->


Haskell
-------

```haskell
acc = sum (map (*3) lst)
```

Beschreibt "Ziel" / Semantik der Berechnung <!-- .element class="fragment" -->


== Slide ==

Haskell-Syntax
==============


Zuweisung
---------

```haskell
x = 5      -- innerhalb einer Funktion
let x = 5  -- in GHCi
```


Datentyp angeben
----------------

```haskell
x :: Int
x = 5

x = 5 :: Int   -- in einer Zeile

y :: String
y = "Hallo Haskell!"
```


Funktionen
----------

Definieren

```haskell
addTwice :: Int -> Int -> Int
addTwice a b = a + 2*b
```

Anwenden/Aufrufen

```haskell
addTwice 3 4     -- ergibt 11
3 `addTwice` 4   -- Infix-Notation
```


Operatoren
----------

```haskell
{- Beispiel: Addition -}
3 + 2     -- Infix-Notation
(+) 3 2   -- Präfix-Notation
```

```haskell
{- Division -}
4.2 / 1.3   -- für Gleitkommazahlen
10 `div` 2  -- Integer-Division (rundet nach -∞)
12 `quot` 4 -- Integer-Division (rundet nach 0)

(/) 4.2 1.3 -- Präfix-Notation
div 10 2
quot 12 4
```


Negative Zahlen
---------------

```haskell
(-3)        -- Immer mit Klammer schreiben
7 + (-3)    -- ergibt 4
```


== Slide ==

Basis-Datentypen
================

```haskell
14                :: Int
37383938373131238 :: Integer
4.5e7             :: Float oder Double
'?'               :: Char
"Curry"           :: String
True && False     :: Bool
```


== Slide ==

Kontrollstrukturen
==================


if/then/else
------------

```haskell
if expr then expr else expr

if expr
  then expr
  else expr
```

`else` muss immer angegeben sein!


case ... of
-----------

```haskell
case expr of
  pattern -> expr
  pattern -> expr
```

Default-Fall: Pattern, der immer zutrifft

```haskell
case x of
  1 -> "Eins"
  2 -> "Zwei"
  n -> "Viele"
```


== Slide ==

Hilfsvariable definieren
========================


let/in
------

Definition vorab

```haskell
doubleSum :: Int -> Int -> Int
doubleSum x y =
  let
    xx = 2*x
    yy = 2*y
  in
    xx + yy
```


where
-----

Definition im Anschluss

```haskell
doubleSum :: Int -> Int -> Int
doubleSum x y = xx + yy
  where
    xx = 2*x
    yy = 2*y
```


== Slide ==

Pattern-Matching
================

* Auf bestimmte Werte der Parameter prüfen
* Erlaubt "if"-Abfragen ohne `if`


Beispiel: Fakultät berechnen
----------------------------

```haskell
fact :: Integer -> Integer
fact 0 = 1
fact n = n * fact (n – 1)
```

* Definitionen (Patterns) werden immer von oben nach unten geprüft <!-- .element class="fragment" -->
* Erstes zutreffende Pattern wird ausgeführt <!-- .element class="fragment" -->


Beispiel: Größter gemeinsamer Teiler
------------------------------------

```haskell
ggT :: Int -> Int -> Int
ggT x 0 = x
ggT x y = ggT y (x `mod` y)
```


== Slide ==

Tupel
=====

Kombiniert zwei oder mehrere unterschiedliche Typen


Syntax: runde Klammern
----------------------

... **und Beistrich**

```haskell
(3, False)        :: (Int, Bool)
("abc", 1.2, 'y') :: (String, Float, Char)
```


Wichtige Funktionen
-------------------

* `fst` :  liefert erstes Argument
* `snd` :  liefert zweites Argument

Beispiele

```haskell
fst (3, False) == 3
snd (3, False) == False
```


Pattern-Matching
----------------

```haskell
doubleIfTrue :: (Int, Bool) -> Int
doubleIfTrue (0, x)     = 0
doubleIfTrue (n, True)  = n*2
doubleIfTrue (n, False) = n
```

```haskell
fstOrSnd :: (Int,Int) -> Bool -> Int
fstOrSnd (n,m) True  = n
fstOrSnd (n,m) False = m
```


== Slide ==

Listen
======

Die am häufigsten verwendete Datenstruktur!

Sind **homogen**: nur ein Datentyp für Elemente


Syntax: eckige Klammern
-----------------------

Beispiele

```haskell
[1, 2, 3, 4]               :: [Integer]
['A', 'B', 'C']            :: [Char]
[(True, 1), (False, -4.5)] :: [(Bool, Float])]

-- String ist Liste von Char
['A', 'B', 'C'] :: Char == "ABC" :: String
```

Listen können nur Elemente vom gleichen Typ beinhalten!  <!-- .element class="fragment" -->


Konstruktion
------------

1. leere Liste: `[]`
2. Cons-Operator: `(:)`

<br/>

Beispiele
---------

```haskell
1 : 2 : 3 : []   == [1, 2, 3] :: [Int]
'A' : ['B', 'C'] == "ABC"     :: [Char]
```


Pattern-Matching
----------------

Mithilfe der Konstruktoren

```haskell
-- Beispiel: Länge eines Strings berechnen
length :: String -> Int
length []     = 0
length (x:xs) = 1 + length xs
```


Wichtige Funktionen
-------------------

![Listenfunktionen](images/list-functions.jpg "Listenfunktionen - Quelle: M. Lipovaca, Learn You a Haskell for Great Good! CC-BY-NC-SA-3.0: http://creativecommons.org/licenses/by-nc-sa/3.0/")

```haskell
xs = [1,2,3,4,5] :: [Int]
head xs == 1
tail xs == [2,3,4,5]
init xs == [1,2,3,4]
last xs == 5
```


Weitere Funktionen
------------------

```haskell
-- Zusammenfügen (++)
"AB" ++ "CD" == "ABCD" :: [Char]
[1] ++ [2, 3] ++ [] ++ [5, 6] == [1,2,3,5,6]
```

```haskell
-- Test auf leere Liste
null []    == True
null [1,2] == False
```

```haskell
-- Erste n Zeichen holen
take 3 "Hallo Haskell" == "Hal"

-- Erste n Zeichen entfernen
drop 3 "Hallo Haskell" == "lo Haskell"
```


== Slide ==

Fehler
------

`error`, um fatalen Fehler auszulösen

```haskell
sqrt :: Float -> Float
sqrt n = if n < 0
           then error "Negative Zahl!"
           else …
```

`undefined`, für undefinierten Wert

```haskell
sqrt :: Float -> Float
sqrt n = if n < 0
           then undefined
           else …
```
