AKIT2
=====

Funktionale Programmierung mit Haskell
--------------------------------------

Arno Hollosi & Michael Fladischer

SS 2016, FH CAMPUS 02


== Slide ==

Partielle Applikation & Kombination von Funktionen
==================================================


Funktionale Programmiersprache
------------------------------

Funktionen sind "first class", d.h.    <!-- .element class="fragment" -->

* können als Parameter übergeben werden  <!-- .element class="fragment" -->
* können als Ergebnis retourniert werden <!-- .element class="fragment" -->
* können Variable zugewiesen werden      <!-- .element class="fragment" -->


Umbenennen
----------

```haskell
greet :: String -> String -> String
greet name place =
    "Hello " ++ name ++ " from " ++ place

> greet "Donald" "Entenhausen"
"Hello Donald from Entenhausen"
```
<!-- .element class="fragment" -->

```haskell
helloYou = greet

> helloYou "Donald" "Entenhausen"
"Hello Donald from Entenhausen"
```
<!-- .element class="fragment" -->

```haskell
helloYou :: String -> String -> String
```
<!-- .element class="fragment" -->



Funktionen sind ein Steckpuzzle
-------------------------------

![Steckpuzzle](images/steckpuzzle.jpg "Quelle: mamikreisel.de")


Funktionen sind ein Steckpuzzle
-------------------------------

```haskell
greet :: String -> String -> String
greet name place =
    "Hello " ++ name ++ " from " ++ place
```
<!-- .element class="fragment" -->

```haskell
-- ein Argument (Puzzle-Teil) vorbelegen
helloYou = greet "Donald"
```
<!-- .element class="fragment" -->

```haskell
-- deshalb Aufruf nur noch mit zweitem Argument
> helloYou "Entenhausen"
"Hello Donald from Entenhausen"
```
<!-- .element class="fragment" -->

```haskell
helloYou :: String -> String
```
<!-- .element class="fragment" -->


Funktionen sind ein Steckpuzzle
-------------------------------

```haskell
inRange :: Int -> Int -> Int -> Bool
inRange ug og x = ug <= x && x <= og

> inRange 10 20 15 == True
```
<!-- .element class="fragment" -->

```haskell
posRange :: Int -> Int -> Bool
posRange = inRange 0

> posRange 20 15 == True
```
<!-- .element class="fragment" -->

```haskell
inRange0To20 :: Int -> Bool
inRange0To20 = inRange 0 20

> inRange0To20 15 == True
```
<!-- .element class="fragment" -->


Funktionen retournieren
-----------------------

```haskell
rangeBuilder :: Int -> Int -> (Int -> Bool)
rangeBuilder ug og = \x -> checkRange x
    where
        checkRange x = ug <= x && x <= og
```
<!-- .element class="fragment" -->

```haskell
> inRange0To20 = rangeBuilder 0 20
> :t inRange0To20
Int -> Bool
```
<!-- .element class="fragment" -->

Das sieht recht ähnlich aus...
<!-- .element class="fragment" -->


Funktionen retournieren
-----------------------

```haskell
rangeBuilder :: (Int -> (Int -> (Int -> Bool)))
rangeBuilder =
    -- Funktion 1
    \ug ->
        -- Funktion 2
        \og ->
             -- Funktion 3
             \x -> ug <= x && x <= og
```
<!-- .element class="fragment" -->

```haskell
-- identisch!
inRange :: Int -> Int -> Int -> Bool
inRange ug og x = ug <= x && x <= og
```
<!-- .element class="fragment" -->


== Slide ==

Currying
========

Eine Funktion mit **mehreren** Parametern ist eigentlich
eine Funktion mit **einem** Parameter, die eine
weitere Funktion (mit einem Parameter weniger) retourniert


Beispiel 1
----------

```haskell
inRange :: Int -> Int -> Int -> Bool
inRange ug og x = ug <= x && x <= og
```

```haskell
-- identisch mit
inRange :: (Int -> (Int -> (Int -> Bool)))
inRange =
     \ug -> (\og -> (\x -> ug <= x && x <= og))
```


Beispiel 2
----------

```haskell
greet :: String -> String -> String
greet name place =
    "Hello " ++ name ++ " from " ++ place
```

```haskell
-- identisch mit
greet :: (String -> (String -> String))
greet = \name -> (\place ->
    "Hello " ++ name ++ " from " ++ place))
```


Beispiel 3
----------

```haskell
map :: (a -> b) -> [a] -> [b]
map f xs = ...
```

```haskell
-- identisch mit
map :: ((a -> b) -> ([a] -> [b]))
map = \f -> (\xs -> ...)
```


Beispiel 4
----------

```haskell
greet :: String -> String -> String
greet name place = ...
```

```haskell
-- identisch mit
greet :: String -> String -> String
greet name = \place -> ...
```

```haskell
-- identisch mit
greet :: String -> String -> String
greet = \name -> (\place -> ...)
```


Deshalb
-------

```haskell
map      :: (a -> b) -> [a] -> [b]
map (+1) :: (Num a) => [a] -> [a] 
```

```haskell
filter     :: (a -> Bool) -> [a] -> [a]
filter odd :: (Integral a) => [a] -> [a]
```

```haskell
foldl       :: (b -> a -> b) -> b -> [a] -> b
foldl (+)   :: (Num a) => a -> [a] -> a
foldl (+) 0 :: (Num a) => [a] -> a
```


Deshalb
-------

```haskell
listeMal2 :: [Int] -> [Int]
listeMal2 xs = map malZwei xs
   where malZwei x = x * 2
```
<!-- .element class="fragment" -->

```haskell
-- identisch
listeMal2 xs = map (\x -> x * 2) xs
```
<!-- .element class="fragment" -->

```haskell
-- identisch
listeMal2 xs = map (* 2) xs
```
<!-- .element class="fragment" -->

```haskell
-- identisch
listeMal2 = map (* 2)
```
<!-- .element class="fragment" -->


== Slide ==

Kombination von Funktionen
==========================


Motivation
----------

```haskell
-- gegeben
square :: Int -> Int
square x = x * x

add3 :: Int -> Int
add3 x = x + 3
```
<!-- .element class="fragment" -->

Gibt es eine generische Funktion,
mit der wir beide Funktionen verknüpfen können?
<!-- p.element class="fragment" -->


Gesucht
-------

```haskell
-- Funktion 1 + Funktion 2 = neue Funktion
compose :: (Int -> Int) -- Funktion 1
        -> (Int -> Int) -- Funktion 2
        -> (Int -> Int) -- neue Funktion
compose f g = ?
```
<!-- .element class="fragment" -->


Gefunden
--------

```haskell
compose :: (Int -> Int) -- Funktion 1
        -> (Int -> Int) -- Funktion 2
        -> (Int -> Int) -- neue Funktion
compose f g = \x -> f (g x)
```
<!-- .element class="fragment" -->

```haskell
plus3Quadrat :: Int -> Int
plus3Quadrat = compose square add3

> plus3Quadrat 7
100
```
<!-- .element class="fragment" -->


Allgemein
---------

```haskell
compose :: (b -> c) -- Funktion 2
        -> (a -> b) -- Funktion 1
        -> (a -> c) -- neue Funktion
compose f g = \x -> f (g x)
```


Operator in Standard-Bibliothek
-------------------------------

```haskell
(.) :: (b -> c) -> (a -> b) -> a -> c
```

```haskell
plus3Quadrat :: Int -> Int
plus3Quadrat = square . add3
```
<!-- .element class="fragment" -->

Operator für Applikation
------------------------
<!-- .element class="fragment" -->

```haskell
($) :: (a -> b) -> a -> b
-- hilft Klammern sparen
```
<!-- .element class="fragment" -->


Beispiel für Applikation
------------------------

```haskell
sumSquare :: [Int] -> Int
sumSquare xs = sum (map (+2) xs)
```

```haskell
sumSquare :: [Int] -> Int
sumSquare xs = sum $ map (+2) xs
```


Beispiel: Summe ungerade
---------------------

```haskell
sumOdd :: [Int] -> Int
sumOdd xs = sum (filter odd xs)
```

```haskell
sumOdd xs = sum $ filter odd xs
```

```haskell
sumOdd xs = (sum . filter odd) xs
```

```haskell
sumOdd = sum . filter odd
```


== Slide ==

Beispiel: Wörter zählen
=======================

Wörter in einem String zählen, die mit einem bestimmten Buchstaben anfangen


Vorgehen
--------

1. String in Wörter zerlegen
   * `words :: String -> [String]`
2. Wörter nach Anfangsbuchstaben filtern
   * `filter :: (a -> Bool) -> [a] -> [a]`
3. Verbleibende Wörter zählen
   * `length :: [a] -> Int`


Variante mit lokalen Variablen
------------------------------

```haskell
countW :: Char -> String -> Int
countW ch str =
  let wörter         = words str
      startetMitCh w = head w == ch
      gefiltert      = filter startetMitCh wörter
      anzahl         = length gefiltert
  in anzahl
```


Variante mit Klammern
---------------------

```haskell
countW :: Char -> String -> Int
countW ch str =
    length (filter startetMitCh (words str))
    where
        startetMitCh w = head w == ch
```

```haskell
-- mit Applikations-Operator
countW :: Char -> String -> Int
countW ch str =
    length $ filter startetMitCh $ words str
    where
        startetMitCh w = head w == ch
```
<!-- .element class="fragment" -->


Variante mit Komposition
------------------------

```haskell
countW :: Char -> String -> Int
countW ch = length . filter startetMitCh . words
  where
    startetMitCh = (== ch) . head
```

![Juhu](images/happy-kid.jpg "I'm so happy")