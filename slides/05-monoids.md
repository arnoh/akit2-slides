AKIT2
=====

Funktionale Programmierung mit Haskell
--------------------------------------

Arno Hollosi & Michael Fladischer

SS 2016, FH CAMPUS 02


== Slide ==

Monoiden
========


== Slide ==

Was sind Monoiden?
------------------

Gemeinsame Eigenschaften der Datentypen:

* Zwei Werte können zusammengefügt werden
* Die beiden Werte und das Ergebnis habe den **selben** Datentyp
* Es existiert ein Wert, der das Ergebnis nicht ändert
* Die Reihenfolge bei mehrfachem Zusammenfügungen spielt keine Rolle 


Beispiel: Addition
------------------

Zwei Werte können zusammengefügt werden und das Ergebnis hat den selben
Datentyp:

```haskell
Prelude> 5 + 3
8
Prelude> 3 + 6
9
```


Beispiel: Addition
------------------

Es existiert ein Wert, der das Ergebnis nicht beinflußt:

```haskell
Prelude> 5 + 3 + 0
8
Prelude> 3 + 6 + 0 + 0 + 0
9
```

Für die **Addition** wird der Wert `0` als **Identity** bezeichnet.


Beispiel: Addition
------------------

Die Reihenfolge bei mehreren Zusammenfügungen spielt keine Rolle:

```haskell
(5 + 3) + 10 == 5 + (3 + 10)
```

```haskell
(3 + 6) + 27 == 3 + (6 + 27)
```

Diese Eigenschaft bezeichnet man als **Associativity**.


Beispiel: Produkt
------------------------------------------------

```haskell
-- gleiche Datentypen
Prelude> 5*3
15
-- Identity
Prelude> 5*3*1
15
-- Associativity
Prelude> (5*3)*2 == 5*(3*2)
True
```


Beispiel: Listen
------------------------------------------------

```haskell
-- gleiche Datentypen
Prelude> [1,2] ++ [3,4]
[1, 2, 3 ,4]
-- Identity
Prelude> [1,2] ++ [3,4] ++ []
[1, 2, 3 ,4]
-- Associativity
Prelude> ([1,2] ++ [3,4]) ++ [5,6] ==
          [1,2] ++ ([3,4] ++ [5,6])
True
```


== Slide ==

Definition Monoid
=================

```haskell
class Monoid m where
  mempty  :: m           -- Identity
  mappend :: m -> m -> m -- Zusammenfügen
  mconcat :: [m] -> m    -- Utility-Funktion
```


Liste als Monoid
----------------

```haskell
-- [a] :: Liste von beliebigen Datentyp "a"
instance Monoid [a] where
  mempty  = []
  mappend = (++)
```

```haskell
-- Beispiele
mappend [1,2] [3,4] == [1,2] ++ [3,4] == [1,2,3,4]
[1,2] `mappend` mempty == [1,2] ++ [] == [1,2]
mempty `mappend` [1,2] == [] ++ [1,2] == [1,2]
mappend mempty mempty == [] ++ [] == []
```


Beispiel: Summe
---------------

```haskell
-- Wir definieren uns einen neuen Datentyp
data Summe a = Sum a

-- Wegen Addition muss "a" von Num-Typklasse sein
instance (Num a) => Monoid (Summe a) where
  mempty = 0  
  mappend (Sum a) (Sum b) = Sum (a + b)
```

```haskell
-- Beispielaufrufe
mappend (Sum 4) (Sum 5) == Sum (4 + 5) == Sum 9
(Sum 3) `mappend` mempty == Sum (3 + 0) == Sum 3
mempty `mappend` (Sum 3) == Sum (0 + 3) == Sum 3
mappend mempty mempty == Sum (0 + 0) == Sum 0
```


Beispiel: Produkt
-----------------

```haskell
data Produkt a = Prod a

instance (Num a) => Monoid (Produkt a) where
  mempty = 1
  mappend (Prod a) (Prod b) = Prod (a * b)
```

```haskell
-- Beispielaufrufe
mappend (Prod 4) (Prod 5) == Prod (4*5) == Prod 9
(Prod 3) `mappend` mempty == Prod (3*1) == Prod 3
mempty `mappend` (Prod 3) == Prod (1*3) == Prod 3
mappend mempty mempty == Prod (1 * 1) == Prod 1
```
